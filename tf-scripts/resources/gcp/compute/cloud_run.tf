#################################################################################
#                                 Cloud Run instances                           #
#################################################################################
########################################
#                Service               #
########################################
resource "google_cloud_run_service" "tf_python" {
  name     = "${var.cloud_run_service_name}"
  location = "${var.cloud_run_service_location}"
  provider = "google-beta"

  metadata {
    namespace = "${var.cloud_run_service_namespace}"
  }

  spec {
    containers {
      image = "${var.cloud_run_service_image}"
    }
  }
}

locals {
  cloud_run_status = {
  for cond in google_cloud_run_service.tf_python.status[0].conditions :
  cond.type => cond.status
  }
}

output "isReady" {
  value = local.cloud_run_status["Ready"] == "True"
}
########################################
#             Service IAM              #
########################################
resource "google_cloud_run_service_iam_member" "invoker" {
  provider     = "google-beta"
  instance     = "${var.cloud_run_service_name}"
  role         = "roles/run.invoker"
  member       = "allUsers"
}