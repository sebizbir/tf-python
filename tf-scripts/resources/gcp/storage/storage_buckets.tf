#################################################################################
#                                      Storage                                  #
#################################################################################
########################################
#               Buckets                #
########################################
resource "google_storage_bucket" "generic" {
  name     = "${var.storage_bucket_generic_name}"
  location = "${var.storage_bucket_generic_location}"
  storage_class = "${var.storage_bucket_generic_class}"
  versioning {
    enabled = "${var.storage_bucket_generic_versioning}"
  }
}