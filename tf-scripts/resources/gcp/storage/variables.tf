# Storage buckets
variable "storage_bucket_generic_name" {}
variable "storage_bucket_generic_location" {}
variable "storage_bucket_generic_class" {}
variable "storage_bucket_generic_versioning" {}