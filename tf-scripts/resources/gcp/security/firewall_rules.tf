#################################################################################
#                               Firewall rules                                  #
#################################################################################
########################################
#                  SSH                 #
########################################
resource "google_compute_firewall" "generic_ssh" {
  name    = "${var.generic_ssh_firewall_name}"
  network = "${var.network_name}"
  allow {
    protocol = "${var.generic_ssh_allow_protocol}"
    ports    = ["${var.generic_ssh_allow_protocol_ports}"]
  }
  target_tags = "${var.generic_ssh_target_tags}"
  source_ranges = "${var.generic_ssh_source_ranges}"
  source_tags = "${var.generic_ssh_source_tags}"
}
########################################
#              HTTP/HTTPS              #
########################################
resource "google_compute_firewall" "generic_http_https" {
  name    = "${var.generic_http_https_firewall_name}"
  network = "${var.network_name}"
  allow {
    protocol = "${var.generic_http_https_allow_protocol}"
    ports    = ["${var.generic_http_https_allow_protocol_ports}"]
  }
  target_tags = "${var.generic_http_https_target_tags}"
  source_ranges = "${var.generic_http_https_source_ranges}"
  source_tags = "${var.generic_http_https_source_tags}"
}
