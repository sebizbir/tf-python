output "network_generic_id" {
  value = google_compute_network.generic.id
}
output "network_generic_name" {
  value = google_compute_network.generic.name
}