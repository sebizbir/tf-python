# CloudSQL - MySQL
# DB Instance
variable "mysql_generic_database_instance_name" {}
variable "mysql_generic_database_version" {}
variable "mysql_generic_database_region" {}
variable "mysql_generic_database_tier" {}
variable "mysql_generic_database_ipv4_enabled" {}
variable "mysql_generic_database_private_network" {}
# Database
variable "mysql_generic_database_name" {}
# Users
variable "mysql_generic_sql_user_name" {}
variable "mysql_generic_sql_user_password" {}

