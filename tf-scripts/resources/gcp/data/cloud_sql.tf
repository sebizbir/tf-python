#################################################################################
#                                 MySQL Database                                #
#################################################################################
########################################
#            Prefix for DB             #
########################################
resource "random_id" "mysql_db_name_suffix" {
  byte_length = 4
}
########################################
#              DB instance             #
########################################
resource "google_sql_database_instance" "mysql_generic_instance" {
  name             = "${var.mysql_generic_database_instance_name}-${random_id.mysql_db_name_suffix.hex}"
  database_version = "${var.mysql_generic_database_version}"    # MYSQL_5_7

  region = "${var.mysql_generic_database_region}"

  settings {
    tier = "${var.mysql_generic_database_tier}"   #db-f1-micro
    ip_configuration {
      ipv4_enabled    = "${var.mysql_generic_database_ipv4_enabled}"      #true/false
      private_network = "${var.mysql_generic_database_private_network}"
    }
  }
}
########################################
#               Database               #
########################################
resource "google_sql_database" "generic_database" {
  name     = "${var.mysql_generic_database_name}"
  instance = google_sql_database_instance.mysql_generic_instance.name
}
########################################
#                DB users              #
########################################
resource "google_sql_user" "generic_db_users" {
  name     = "${var.mysql_generic_sql_user_name}"
  instance = google_sql_database_instance.mysql_generic_instance.name
  password = "${var.mysql_generic_sql_user_password}"
}
########################################
#            DB SSL cert               #
########################################
resource "google_sql_ssl_cert" "db_client_cert" {
  common_name = "db_client_cert"
  instance    = google_sql_database_instance.mysql_generic_instance.name
}