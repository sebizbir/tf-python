# EKS Master
variable "generic_eks_cluster_name" {}
variable "sg_generic_eks_master" {}
variable "generic_eks_endpoint_private_access" {}
variable "generic_eks_endpoint_public_access" {}
# EKS Nodes
variable "generic_eks_image_id" {}
variable "generic_eks_node_instance_type" {}
variable "generic_eks_node_name" {}
variable "sg_generic_eks_node" {}
variable "generic_eks_key_name" {}
# K8S database connection
#variable "db_instance_endpoint" {}
#variable "rds_username" {}
#variable "rds_password" {}
# Subnets
variable "private_subnet_cidr_az1_internal_app" {}
variable "private_subnet_cidr_az2_internal_app" {}
# Load balancer
variable "eks_load_balancer_generic_name" {}
variable "sg_eks_load_balancer" {}
# IAM
variable "generic_eks_master_role_arn" {}
variable "aws_iam_instance_profile_node_name" {}