data "external" "aws_iam_authenticator" {
  program                   = ["sh", "-c", "aws-iam-authenticator token -i ${var.generic_eks_cluster_name} | jq -r -c .status"]
}

provider "kubernetes" {
  host                      = "${aws_eks_cluster.generic_eks.endpoint}"
  cluster_ca_certificate    = "${base64decode(aws_eks_cluster.generic_eks.certificate_authority.0.data)}"
  token                     = "${data.external.aws_iam_authenticator.result.token}"
  load_config_file          = false
}

resource "kubernetes_config_map" "aws_auth" {
  metadata {
    name                    = "aws-auth"
    namespace               = "kube-system"
  }
  data                      = {
    mapRoles                = <<ROLES
- rolearn: ${var.generic_eks_master_role_arn}
  username: system:node:{{EC2PrivateDNSName}}
  groups:
    - system:bootstrappers
    - system:nodes
ROLES
  }
  depends_on                = [
    "aws_eks_cluster.generic_eks"  ]
}

#########################################################################################################
#  File CAGateway.yml from CA Gateway website was translated to kubernetes specific Terraform notation  #
#########################################################################################################
#########################################
#                Service                #
#########################################
/*
resource "kubernetes_service" "gw" {
  metadata {
    name = "gw-svc"
    labels = {
      name = "gw"
      service = "gw-svc"
    }
    annotations = {
      description = "The Gateway service"
    }
  }
  spec {
    selector = {
      app = "gw"
    }
    session_affinity = "ClientIP"
    port {
      port        = 8080
      target_port = 8080
      protocol    = "TCP"
      name        = "gw-http-port"
    }
    port {
      port        = 8443
      target_port = 8443
      protocol    = "TCP"
      name        = "gw-https-port"
    }
    port {
      port        = 9443
      target_port = 9443
      protocol    = "TCP"
      name        = "gw-pm-port"
    }
    type = "ClusterIP"
  }
}
#########################################
#             Deployment                #
#########################################
resource "kubernetes_deployment" "gw_dc" {
  metadata {
    name = "gw-dc"
    labels = {
      app = "gw"
    }
  }
  spec {
    replicas = 1
    selector {
      match_labels = {
        app = "gw"
      }
    }
    template {
      metadata {
        labels = {
          app = "gw"
        }
      }
      spec {
        container {
          image = "caapim/gateway"
          name = "gw"
          image_pull_policy = "Always"
          port {
            container_port = 8080
          }
          port {
            container_port = 8443
          }
          port {
            container_port = 9443
          }
          env {
            name = "ACCEPT_LICENSE"
            value_from {
              config_map_key_ref {
                name = "license"
                key = "accept.license"
              }
            }
          }
          env {
            name = "EXTRA_JAVA_ARGS"
            value_from {
              config_map_key_ref {
                name = "env"
                key = "extra-java-args.env"
              }
            }
          }
          env {
            name = "SSG_LICENSE"
            value_from {
              config_map_key_ref {
                name = "license"
                key = "ssg.license"
              }
            }
          }
          env {
            name = "SSG_ADMIN_USERNAME"
            value_from {
              secret_key_ref {
                name = "gw-secret"
                key = "ssg.adminusername"
              }
            }
          }
          env {
            name = "SSG_ADMIN_PASSWORD"
            value_from {
              secret_key_ref {
                name = "gw-secret"
                key = "ssg.adminpassword"
              }
            }
          }
          env {
            name = "SSG_DATABASE_JDBC_URL"
            value_from {
              config_map_key_ref {
                name = "env"
                key = "ssg-database-jdbc-url.env"
              }
            }
          }
          env {
            name = "SSG_DATABASE_USER"
            value_from {
              secret_key_ref {
                name = "gw-db-secret"
                key = "ssg.databaseuser"
              }
            }
          }
          env {
            name = "SSG_DATABASE_PASSWORD"
            value_from {
              secret_key_ref {
                name = "gw-db-secret"
                key = "ssg.databasepass"
              }
            }
          }
          env {
            name = "SSG_CLUSTER_HOST"
            value_from {
              config_map_key_ref {
                name = "env"
                key = "ssg-cluster-host.env"
              }
            }
          }
          env {
            name = "SSG_CLUSTER_PASSWORD"
            value_from {
              config_map_key_ref {
                name = "env"
                key = "ssg-cluster-password.env"
              }
            }
          }
          env {
            name = "SSG_JVM_HEAP"
            value_from {
              config_map_key_ref {
                name = "env"
                key = "ssg-jvm-heap.env"
              }
            }
          }
          liveness_probe {
            exec {
              command = ["/opt/docker/rc.d/diagnostic/health_check.sh"]
            }
            initial_delay_seconds = 120
            period_seconds = 5
            timeout_seconds = 5
            success_threshold = 1
          }
          readiness_probe {
            exec {
              command = ["/opt/docker/rc.d/diagnostic/health_check.sh"]
            }
            initial_delay_seconds = 120
            period_seconds = 5
            timeout_seconds = 5
            success_threshold = 1
          }
        }
      }
    }
  }
}

################################################################################################################
#  File CAGateway-config.yml from CA Gateway website was translated to kubernetes specific Terraform notation  #
################################################################################################################
#########################################
#                Configmap              #
#########################################

resource "kubernetes_config_map" "cm_license" {
  metadata {
    name = "license"
    annotations = {
      description = "Configuration Map for license"
    }
  }
  data = {
    "accept.license"       = "true"
    "ssg.license"          = "H4sIAB+ub10AA538x5LsTJolCO7rKUKiRGaDyQI1kJyM7AFnBsDAyQ7MwDnH0zeirv3VmV3V0jPtvjFxA46Sj52jqq7/9r+dXfu3PZ+Xauj/8Xf4v0F//1vep0NW9cU//u46wr+Qf//f/v2//FtbpXm/5H+Ts3/8HcdhBKUonCRgBKIoGCeIv//tgemXf/y9XNfxX0GwJdY8Lf9bOnTg782///t/+dvz829ZvqRzNa5Pc//+Gdi/sUO/znG6/u0FQcSLfOH/8rEMzmUd2dD/DfyPT/95/wdHr+tcJduaL+Dviz1uq+zfEQim/gUi/wVBHBj+VxT9Vwj+byiJRv8G/vn+z7P5OVZzvvyepv4FQf/PT//1xJ/ny2FZ/9bHXf6Pv//9r/aq8W9xlj3PLP/hj+M8ZFv617Pv+MrnvxF/s/N0m3N7jPu/2Vu1/jUXf7r9Z+r/1sX1MP/j79Tf/9ZV/T8//YX535/65vH6QCz5X9DPp3/9zMO3avN/5fs1n8e5WnIxXvMjvv5Hd8Bff/7z3OV/S59Jf+ac7+Kq/cffs6Hv4//Wxeta5sfy/83O9J+G+/uvJXYejp4u8n5dmLhv/gd2vrVPa+f67xK2yPRfPwZMXT7WwERQ+EVCcmfKSyWmMGJ32dN5+fHSdPGtKhCedZGVoiuibVqjeb7Fq0V8/b/+65n9f/6LW+3UAS9RTsc4ZG71B6fR2sMoMOyAUaOgEis0Z46aBeMlUZJPGZCN1/OzLSmnp4eTeUjkjki7pM38B3E4QnR0hHdDZdMGY+GaBwC6jhaJKy5gfE5TCIT9TTijJ85LU8WbnHyzGLXtBda6FRh0lZp63+3cZLT/IGZX8iY/4OnCsGdF9ySOWGNld3G8R6OCz9GXawNGxovsff59ZV2/Jc+Qzub0IJ4mo6ghuW+GdJQtafEfxAvCqT30wOArfP17JXYQ1ROUujlxS3S4Q7wpsNgEAt63HLJtFnSoEkHheTZ2Ot4tyOR3jjsf7ty6Uv2DCJv0Us2Nag1xS9OshlobULkaJJjoiFKrE8/DUAFR5zqmLBysSiMqX5BaTrP0TcMfw/F6dtwGdiVu+A/i4leIoDGYfRAcIaJlMBA+Fp3C5rRt+QYNeBquLmxnAGXmk9H3yZP7OBY6x4O4kfarjaFpvsGPTzZafxBDQpsc03u/VLDUDFdorRyGuil6Ne4zOiwAyrhgU8YTU3pxCeEZYrXCIFb2geW8scH+Ykl2ewpWSqzwB1FIifFjqo+v4VfYwarRi5TfeeMs5lH1pR1J1j7VgJWloQSbaIXkLabd8yg1B6bGVaTkEpCB0Ro3EskfROYL0SRLrFEhocmAUi9Nag9J0qsi9LmO2WYk+txTwYGM5hxon9HoSZok7kIWdRhO5J1xYYkwFIl7SP9BbIwW49XWkjZgTFwK2IwlTpM1xqILZUqacr5ckIcjrx9feTIboJj5D9e3sUWQZ6KaGwyMKDmBrvemf96TJZmMQe4bOyHKO2D2uEyiyWYASL+gwRAkaOjfrOes9WbmKOokOZ/45WYmAKElpgVW4zaf/k7m2tHAH0QpzYQuL8h2AyZmSr5wvU8AlNxgyVhrlxKboXsaudtxISbVIY6O0k5iNyiDOKosaHdQnSbRe9UxYJj+IB61ViQaYUfe4CBuMH0lE64y54Qdl5xh3Tl2kjGIq/2CjH0dI8Ilq5ag78p/xU4eOCyYseFXSRL/+IB/EDmlmAstBegrUncRX9/LwtdbBanKE+dkMD8jjrFT96I0SbuWEF9naZNRf9BX2ANCvo4YCxHtebjzvv5BZEXWJF7zRxMxodN0EJ4CQwUYsTKoaawWDLBlaeTlcLx72jgytr+k2rRvgbPTnBC0wr2nmd6xCaD+yj0SPeRvH8uE8l6kKgy71wYVpWtSRilZiy9Y+3Fv7jukj7rworl8T5BZmcKdeYeDGgPx1eC1xUzlnj79H0Tytrj0q3nf4RpGBWnou85aaRrrBlEi5sP5t7wnwsvsyCI7i7bYUj6t5C35vEOWdvFFFVIhab5FepA/D3epVcQuooyvJKzro9A3uVQe4wnW/Yp1rHUBa0f8qbQtNvSrnQ9pu+c1Q9XP3UrEFgDRG1qSWz+qIv2DmAwrUnEWFKJvWijtA2jjXsx5nVM0WmAN3hdk2ljVd20m6TTA5RSqDiXBCb2n/LxljYwat/hmjIgXsT+IGu9rhNrVmD2qJIo47V4UTxcJOp2LybOrczm9aiQLb1GB6plB+oza+kmIIQaI2kwX9mZKM0/HkRV//iDy7FvDMOYrEzLTLuxgQ9HLNNxQpW/rDAiAfErOPDqvnVZEBi9bdcfiFd7pBDyjjiQZvzdREzHNLzm9/yAaFMcn271JHE2oxhtTInoMGef9UTnolAwWaW0eVhfxKQm2aLEkyckLi2a8wOKxmLewquA0Q3JvJ8LKn60hbm18hpsGVVJ02qW1pVHkiT7hFTenUXAG7ua5L+05NGpr17IlLdMPJxQdgmh3nrgTjb6+An30MOcPYr25kxrybssepBycTGR98kJE+YKXB30o3gJOp50+Q4XSdIUHrZT9ZYEowiEvXP3vfNlaqcinzXIJe/5B1H28z75VUyhxcoIoDaXSw2YSp0iGOHi9Ugki+7imc6wEvzWPiwowX2YBbaYqfiddaOwT0pVKM786YPxBHEtym/HvhJAC9nlXYT2ETua0PgZIXs4VUdAcEkRJLfM9IXakAE8APu8Gq/TQ+pSdBoOsAZg48GRl8Ido1RyHPxYhPmOTfaS8RhfEKMFTZXidfLLcgwYlcv00hu1ZkwYjooVJXxgzcJQwahTWKw68lEVEZv9VVwWiJZ5bDpVKgCzcPVqEsBVG3wZwf5Bdn10JMUh5Gz+SuvS6WR6DRrQ0gWmo4HVauBpPLEV2fczG8Ivrp+adE+rHyJ1+EY0yYBlQOOjpTPLu2oQJlQKSSQcbq/dFX6ngKrVgZaddA3VmyFsLz1BZ3Z7gNpjl/Txcp+HCkRnIiPzoQPeesPV+hlyLfpeD4FtiLvDHN/dF16OMKQjYga/CbxyqiXxZH+eg+7umRMd3apf9gzjHd7Ad7VEFdB6ysBDOXeg1Da6Qwhe58kG7243ZWN5JNP0Te1lACYms+Y8j0qPo2ZEoEqUKiwkfOeMfxDt+b/SRDQPtqY1S8RJIAw3/0Zp+LbqozJrgXrVAhcpt6izhKGqzX2tEFUBmHs1QfyYY2gnXEAVE+XGzfk2uNGVfrOV/tbNUaOC9V485OrV8+fadDDznIzYLR0C3wKKNVnIbwDFzHHbTv+pYZ8xB6es6TI+u/vnjlfGQ4b3LYpgav89pQXK9BFT2i7heMu/IrcgOGJuaR2GCg60qQOtr9vJFWpWD/X7R9fAgkantyuXXRz3+1FjqNYb8ODblwBFfpOTyMdO+hNMBJZsAj+sm5zTUhSWZ3ApL/wBrRWHkwCggLn8/Dd+ix9WV1/dXC92kl7nEuBtFBM0yOcluZeg6AvAGlzD88yHZb4F/LMSv8cYOccmFdle8UnpYJNtxQwf6SjCSc0sy4b/K1VKZM8SHJNQ9L4Q0iIHzKsmdqE/KakhR9ClWln2GRvsLjYZYzpH5Mpf0UxRaDtGr/BiUXFQon82KP4ixGAhVXTfw1eAtG/YBAq4lfx7QayjvYhg4+8lZx4JvfSAPUX0b8sXyaXNaH7KaFdEzhDQmPuek8POvcn28h7rJjmkVbbcG9Wt+uShyjuImWyjuwailyp2F2vPJ6lb3Wk0Eqt+lWiKEA7CGFRe3zoJSmOqox6E/f6ytPd1EoKMApXJf3f3QYiHzn/zR0tj2xC34TUqs1feK81FwQpjn7TJv2HbYLkOB4FUEq7V36IkWfkzKnEmP2FgxUaom8mwUdgbwpRZtaHQldeBF4EIzbnnMjgFLiddCqFdsGEMxSZu+JKYFWSmYL2EXJ+W/PrLAh1aEOoR7xDJJySj14GiGilkylbnJs+fvFF5swfrEgwnMZl+dw8iuWJWBpfOa5RyptHw6AX3n7+5XXZGG0FqCJvMX3NVF9W77/QACA4v3WPziidqQLa0H7J7BDsoAhJof80uwRgPpdwkjNXulbShToYr/Dn8Q3yqG5QCdXU3W6W+o8foGeLTWAH6GFNAHF2HMKxHZ9vnQWSRdzBirSwVgmLQGiOj97j99lQ5jmt3qj9l/XlxAQyTvFA+nfWP8XU94wQIWfcnci+4PChQKUOFf2kmEqshGhmrJraX1kjP6h5aZWJzDEilHgdH89Ezq1hFcHh57C72foFxAbYYUk2+KWAejIaKrM6MJlCs7+PSf4xTUeR4umW7eSN1dCfyxk6eYy+B95P6vKkxdYkKWqshHIbFwA+P5y5fsVU90hth1W6bu5pR6M2v2F4PwM74LCP24aE5wuV/IQ0nIA71RyiwXw49JjUt/chl2Ieob4U06oN5dzOUJYisiprpVP6J5decA4/KB42XbTqGpO70XD4cPluRs8O3T90vXlNvDqz+IdLxqTbHMOvo1XtnbGng2oi/WS7JYMS73I0iG3uoWy0hoo78VHpYSi4DkIkMSs7Cthngl35gKtL3YfrZOK3eQlLGVnYkLSprjAJTjkIiVLrc7yjELdWqZ3hp/cTFLQ8Va7/kpikCLWjaKFbjNmB9xhrjQD0n5x3t6CkKF/HLD2Ww+OGPIPOm9bB+qa3Z4BcnGbyRtdFZ57LF7aIW+49rI+dUnPMdM6/wO04JO2M+iwH8KKXx/9FUW3uaNy3IM0kJIILp6pZadGnupKQHERYW37pU/5M1ejwXOcpTTRLT2yfD+vWrfjClTiNvM7Zcf62Yy0HuB7J1NrIc3kqQsmUyq4CYGz6Ury0E20fqbdEwBtKz1orMuSkmilgamuRhOfWdhwKVrFQjA/QfRHpyg8gmeDgvRd6e4ChsunLpSVFZiA3SwZ/q3EjZ0HN8GJ74htidZU++x2xo+KCZ5xdt7D3ZSMWeS/aVdYcV+n6VYtEqWGc7e8YWVl3MWLkJOzWnjantCVb14TJEwGUkGF91pD7rs+YWXOfiC93gvhQJf5j9E20T8l3QYC7/FfnN6QDO1NmoujZqeNLHZ1MOoNVm8dPbidoqbWWOcE9YXTUxDfNc+MNqi4FK4o3v+xcws1yPzQkvNiQc0lvYjpjfVehmZhrmT1rCOIX1FYRYyN5r8Q96fsbKFKTUCYJmtz/HuxDysGXkYaUP8QcQzepZlzMXeJofwDCT1w8ZHZsWoM1HjpMctPGem1Di2RiPqHNVWS1ztrJxnOoUJGOKlsNbB98qJRP6L6zUrbdBRQommGU8bfAyTtDn1/Av3IcCceztcxx5ATY7iQnRoy/QVNYf7pu+qfTe82uVkImENDUbXL/e0skEdKMiYsPAe1ErwkjQYWK4YD95Ox4u0uIevfGTk5L/+G67CZU3Zgnp8iHIvfeJgumIODvJNaTWWP4gEN3zdqUwgUvMxRnqZsrIn4iiXLgZ7ekv0TLgSDYo9DBmzTBgghiZHJ9HdG3d3SvFONgVj3oKw3hH5U3FQnL+d6s2OLYMZiRu3sU02PIQk2fElijPs2+ohd51Rkj02ml48cAIMrI9LBiUTXIm3YvqKjHapXz81TDFywtkvCKDMW9G7SJ5RFicZ3fU4ZpN2DJWzL89zwkeQQRAsvl/zi/5Thx0m8yKKD+TmOU0YkdOCx49T7JiUxXDYnRebVoTpF/SciLzRI5JQzW5QJJY7CI6ovubO4INRhpstoy+ykapeqi4BBmv/fMgFcYoT8wdxhRo0KDnUmBroXQC1Q7378tH98/eDujWuWmUR8HXiC7RsBp74LmOwvl3bbueAzR2WcR9/KOZNUEz1548qyFPwSo/y2w4Ftwasah+Pfendm17x4WTNnjaoiao/clhNBF7BPu0zO1g4RIddhyRXqCX0bQtU60L/5Y8i3JqABIAgm830UjRFoKpe45Unae2iZI55I7zp8Sa3vIkBRf8KGkr0n1VymXdY1mPFgpeS3TVo/PwxxidleXkdO6NwEca0x7FoMZWzQMTlJrIE/THrBjmHVyslTo6zd3XgfBsrPR3L1fI4OJWIZdoLS6j+WIphGbzxuffT+/SScOY2FDahNJUk83r3SSnXev6U2KSoeD+RXvIpyt6Y2SjqVJZqION3EhBX47apVuxf7nEzYvh8YAZt9uiFfa/VPXTWi3tvpoX3FAZpcfrkMTWYYpEqfdI7Vccnqx5avkmJaBU4MXYZNI08Zmw/y+xNP01IO+UQP78PWh7EXK98bq/gUd6W96GKTGmZyojspl6VpxJpYFHUnDEpg+kPXzoz6SeuE5QJ2r90of8yL/+SVxO29EaRRB27nhx8sUVxhEe8MmPzjutam4MvjafSQVoPQ0NSTqeHLu8xF1rfXHlpnRX/dGEIfw+7xPUGbyp+vpAreHSSRJ8R2QiqWGZV+uKYXfY+WeNzDBx6VlWZgCVOroMl4zc7H11R+8PL6vNfvf5EjCbIcK3jqx6Kj3xS0A+PI8GNs+zLyQ1lQlGu/bBFf7+9zbHO51f3/PCtNBEIxsOroe2Oa4buYH6jnnqOrRq+5C+FmYgw3YRMlyviuqLaKyvxFed3YjLMvYVnZUsfI48eR3Qne7pFjZ2HRhRIYS6NVVj/sjXZzkZtljsESuZLls63QVTljKKrqukEqCBbaWCtZdwX6XB3F2WeHynvSfeZMhxTy407LqdL460v6fqLGR0AVcMjtIiQMf+IrsvQ2tz9uB+99unVPD70B/mG/rS+CYJtw/eFwcpUCioqG4dlr1ld4xxBjFWJcL/K5TsThTd7ozPH9cLP6uEhGF5F9iAEbjDxDFM0R6KpIIudw3ttYwEgC3F0RgUxnJCKF77IGwJoQwZsqZ/6qD6sGrV0MBCL4SJ8ieB4F3I+4zQchk75MgSp15mHYeMUmoi+qeUcYdoQou3ARVa5BqvxeVeIEEDQLwpZsyTQ7KysePlyeVQTO21vmmAX17fnpF1qx141X9Vb91g0ay+TUWbx9dKuFrApPmtio+yP+AA+UvtjUoNQ1732ipf9jNbDddVAEaK2bIfDt2BGwfgFPsQ3Ju/ZlAuNdZ8yY7dPb78PEyYiYAYnWNivmKJcWfuDGInMGSMX0sJaSWpnMS8DQeakV5ElromumjL8wFpQcopq/AqvFQcxqHA6aesdgQ2PE2gJf6JRyYrCvxjpEuim+qVs3DJNmT/vz0jtPLPCQmsEqKc5jYCkIWjtmt0kEX6YpOEfyfbilrm1rSkpAYGMKrs2EvW3tkcDqcj5E//MkCBbBSfrZWg6xmzKt2N/1ZcwFqtXzmfRreoMfT6lsCm1lvmlqcOzpJEawttowLTWZf/W9gZ+vImzhWxqPd419gJCvYEzwAZToU51Vcq5k+BrCFKCqOKHFWYC4RpUC9ykr6mzJ91edEy4M4LWt/JTmmojS7zjW51SiQ+b0rvdH1UCVfpV1nBJrA9N/8AfBGdpBvrM7kw8kemoyuFjX0mda4gQcAAdQQpAf/r6jWYY/HAZ2+s4vluaMrMd9T2MNhzKHA9w22w31FwoxKrLCP7UUGVx32jCyvYGgVc823DZXhH+fgT3j9n3hjJkF2VfxrqYfSpv0bEy2SjvHPdu9SvXzqrbVIEMwKJJAFnrUwq1HhGWH1SqH4HYK62PckNVmo7/B1Gc8NGU1KVgOnPK5IuhIIWENhk6nsr87tEnl376sOBE48nIR7t403WDnXVLDuoKtVVa62DfmgYPjfyr1wJDVh//oXla6uoabUfUe9AhcS0HnD0TsCCtWGO16F0bJs+J0yvI4SPsE+wwPBWxL+2dEue7GpxSzn7c7FPZ6YvtiVF36RBePaQRTN1vO+Gtv9YgP74YrVGHCUbx9y3tQ7G1AwageFQXmtfKjSgHiNr4AsbKxU8rsLNaMq9qyoPhJW4nj4+MifflB8yzL3dQ39AOH1SsZMu3MH55INoFk96YgZUUzyM5ViPp5mhOZN30325Kd6ASFb6wGoBEV4ON6kiEkNz1WhKhQ5iPGzR1RLaI2ZGO/ruLyJemKI70aHtgjDOtKMJbBNnL4Sz8ZbM3JDOZKVWcZVkIsmSOxXlLbcTtIzeot1dxnh69xHaRcHIkAhczYj718IOxgS/V+fgaV45S4SCpGfVvnYLhF2M44CKv8vGc1SSO95CDsSiwzq8zfGsSa/E1AA7plvDKGGvWxlMvOwPcHwXSU8yMZzs0177DzP9U3FCLcDCpG5ePEXHdB+enKcTmL1imF7pWbY2OX7Kjn/vDmmBnuE9hRCtPD0ODm/Ol5krBhV+fQJAS+ren+e4Z7vWMz30z6phvfUEqeA+mh4S10NHuVhHshsh14+eF6oauic2GMR+Ik7wDwH1jHEY8xXlsacw6PX7+yBc3O3ARoyrxF4ntprawVdr9wSgWn/K2NhvxGnKLOD08FSAln6DTW2O9VKpR/KZdeJNeNmh9qMh3f1VB2boOkbWdUGyvyIR4TgwTMXetTDt76FR06yGvNh3WJjgt0BlGXbHKUl4MUwmuL+NGmKYT6eNyJ/5yz/nkocbAeOwhIlGLUs72JAThbqRvmj7kz3QDpMxph/FEtOvWS4zcTEde2pN4mlmYQgwohqQ/aUMMu19cK7JsuqygIL49cN8NbqICq8ob91ZWp0reEWRZtnzWOXD2iHQTs61VZaOqPIDDorCzkTLcyRphkSn7V2eMD2PYB1nbSTFS6nBcCK1PoJ1vtOv3wLuE3FBF5LJlcOJgIlf9pturKSMV8y23tr1Wt0zguM9nUPdvX8EsaaoRQ5ntDr7YzjSRvcOsl1en2ClSn33DlvUR6g+jlgpmitrqBZvDx2zcgyENJtpkwFXeJ9UMqvlbD6fioG8IZUqBJ0/ZCKZJ+fH5mmGO5Z0evgRZDQGal7TRVz3IWDNCxxYXn3xhRp6eppMWbErThwidmr95ZJ5+9bGDcwVO2osFJBMUjIQTQ7eYst5ESvrUoNqr47Y3zt/EQ2D1aoVipuhzPrNpV3uDbjuCXvxmf5VLaSqfnbJknQYsET3HX9dm1swlvUyNgNgq7iADL8OZeojQ2MQMLPmp++byLX5SQA3zZkMJ60ps++T8VrlkJp0Bfkr9rgEsWD4lmiXupeuUpfT46l2wypvn3PdQ3mxwSCLBLOUHbtNCTt0Dz1SeH71IGt1PZvK/9fD8LZSXEkWRjPZjnEzhaRofs9JN6niE8DvdEfadREbRYZtDwx+GecOo3ogf9c44b6XOCm20+L1gs6GaP5YCrNukr2p2CCUmF+b+kGb+a6v38IhY82qO8pHtc0GHXqKRd++X72HLa0LeTi5Ck4T8SOawFofHX81PIe3svl6Paz8iEpk09Ti6yanE71CN6j3KVnNuWrci/rjkxAmsg3OGinQhLu+RTc+fqnJdzseVTWTwCf6XzcJbFsoCrBnqYvtoT0y7nDUuPYcqla5IW9/K9qXUlhey476/ItbAEaGpPq8zd61IqM+8KTvws4x9/eJ60gZngpIEb1MD2HvyvbTe6F/YPM4jXwonf14zhpZ1jLX5sg1LUtd56QQQbuIUsSJMK2pcrQdafPQ/Hv5EXx7gmkppiEs+nqo1rg4NEK5b6cqvZO+uoAox/XuACjzpLsJsq6BwxS3Tn7xkSksW5lBfeaLQAz/LfOUwFHnVYdOUd6ZHyKCGGti+QWMASGQDHe44YvK0Snm3Kq+hZLXnU0sDIZ5Mg16qdMK75NLJEUmq/Zdxp6iKysYlVNl5ZwFruCWzJKpkXaPVHcEZg2lNbo+/qdYjpXXIZ5rO+pSN9gpd73ESjpQU8DzGUGOln2VyelMJm6m+0dudPIq7poc7g5O5+vtTaKhs6x/eNqD/XJXT2ebCX7dN6Urhr8PL+b6LfgNUpVmGyOx+O7nOvOo8feHTu1lLz4aqlvOJCIt6Y1asoIR3ucCQ3SDbwC7Q60O52Cm9xewJrPiz4vAy0yqJHIQP8d3v9MOhH6RIRBMQlpng1C1Fu2NsoJcNV2p7RtknoFQ/z7X1SQ+diNl5xCRSPB8dbpuvSYmXJ0Ou6Kl47/K3c4YhatXOpU8rdylf9ZKS+JFvHyitT/KL9Z7UOrQW8xOetpDGoDYGSELGmfk065YvCmgemEi+ke+pE347kKKoHeMmW+qX5XryY8cVBg2mBObSiXysltIwa7+HtNcMGi0oPhJiS3vDECyv/vtgNmcCpFNgqGJO698KUi4NlKF9VCKVon9ymjb8EOqXQ0lKaFI6IDZm+sRSh1ydBWkWyirgQX1KwFhMq51ZfISUCxR5TMIw6nfS5SPNYOjMJVzXxKGMxZ4asxqFqjNETsnES4wyEJBCx7K6x0hlfQQBw/rtOCLUodTuHaCfOVtCiTAof2cV7mCHOBwwJFFToLQxV5QmFwUxrTLHSINHN23dLYKpjY9bW8NtQLlE05LGECZdfu0J5IO3GSB50EDdj1Not5v16cd9vdLgwAAr76lXHiq3bfDDoPfHDkO1sr/H+noZNJufeu8dn77+QMySfTFGOM5EGo4mvb719RezZ67a2h+xG0E0Y78+TJcdUxtSdDkF9ifuy8Gy+qd43WR8jADH47SaiFhBkG/WOpQPILYXZ33eRstwfxA3n26Um9E/x9VR7Le0IYYDd/t4phMiy3HEzjdjJPPj8jRitNLBHuzu44cQwsjxVQp+uHgsthoNTRvpr10p/GHWQlRWqe+Y317TQDdpdMN+oWzhCFkfIPyBqWOaVAf9qYfhXuM3X9OHm9RPbQxJ2mToMnMMeftFIeSbuFTCmXOBEsmkOcgFBSHV0AjwSooNSrPJ4fXPERKMaozTp+auns65WLJQg4HJT2chaS8fZjnov3qtOhJNiYUsc2V7N45IkE37z4MJme71YOcsStHVN/bl4mTJ8+LE9HNwclx84cmWy12PSzwDfEMxjerrtwO5N2/6OL5HAtY7bpWFdd7twGz5q7PpeW91v6NDH1H2VkRtuRrY6avYalptscUck9+3n4+ErdNstOU7+q2wz0bhPeHxhla/ww+ajj4Ryhrq12I//GlwZp/0Uz50m06gzTu+YZ2ydCLLnEfkQZIdj4LN3SRDwHH3854I5GerB1fqoyGKtvxz51OszsC8p0kl9Bh8lOtrJJQIzo7WyufN0EWP3rDuSYhc0uHM6dsVhwt6KYi/Pk4F/pTvUM6gl9bqLfalV01+quLlwVtOTSawpTOZOroyYVeMq7A+hpuaKgzavLBPPkBozGF0np9AMP6yGcwv4zLvDVK93CIo4rLYKV61aaSxNKqgBZ4kMM/E8RmHFPCfDNBZqeA2+f1rkfdEEq/7kEApaXUG/p3QmDttqno0uWK/x4YcB4P32iFKyYVlXpEfIyFlqaNmAnIqa6ZSrJGcPseqa4EYpJk+/vX2PrPNMBQF/7zHudSrMBsA6jV6epnJ4y5Wj9yCORNIvwx5vDGmCw0+H7gl3RtGlh2dfrdtwC+EuU6gHooUCWumTyk/D8cC0B0VwVmfon6VhL8LH3IInNHb8+FaMRx+Qbso9a8Nqp/8Dc37/tba8FSg01Tn3okNqN11nKjyN/OLwhCCRa3f1EupzMZAUc5O+op+PZKyXRPNiV1fcjRqjXnbKp2+hNq8+R4KlntW51OaCJqKnEnvmyJL6LfeYwDVcTwcfnhj61VdL/MAtS7BuOpsAuw1lxJzIl9bYOqh5y0JqhsV1wu8E1/6XBJJnI6R1KRUb85g8WNSykeEvNTHVEuQjxyaffGAgbSBSH7gtwY62XJwMgmF12FrVETB7hqLrLuMh/fcUjfeQ8wKfRo+Bq7Pj6U8zOhkdsfW+Y5GzPMkwtKib6YOL3TVAL4xeS30tbAXzAvxcRTNY7EZslfzUacY/DjfC7riIB13keZ+HBdBq9Y45s2TomhzcFFfAI5VOU/TyPd+uwV1zyaN3MN2w9kdwy2fWKHG19WtjZVR7PHbFZJgvvZGSX4MIPxMAEz0vRKe86u0uumb7gNboKrTpSSRg0CoxKIuT5s/t5hvQFwcLvKMv08FLRbG+fZxdifqq/8i79/Kh1xDq0zve8mwpXgPpF/rrjJFqsCtuT93kK494qVz8bYyO1NzTfs7V2y5jF4DvJXA5vGvwbEWkkFa8jsJmEmx1IQmDXjnrYSPji6xSUoh7lZNoI/vTfleXXoisAF3Z3xzbfh2iE/3ltwQz8eUuV+CixDGzXI79Rv1lYE590Sv7L5LujT5R4yZlobtb/EbG/Y+RtXmGTl+IMUMMhuHJrpW9ovdBfgzL/hkAZtcfjaHw+L5d74nXxL5SXlw+rCskUadrellzOk10wIFJVbiaFT1locrK1eVOglgwZYfDw1U1moefMeK+6Q0F+AooL/ODJ8+zxI7U66+2vNDOqkRhp4+XW0XshWkBgnNLB3DLB9aWUbBa4yXHk5wBDlv8MsGDj76WmZcCFit+k9zNYVOtBfJC0JshLBart0L/bRYBQJwYACy7XwtG4UKJb1EsHBVohclMPZ3/46WmD/TsTLJheYPI1qT3+5e8V5OXyN7Coyhx7n8rkgNOyWL7bPcW22tI7FS/HB8CsF254xmCH3SM2VcUpl/yQa6OUrK5DW5Z+/ut/exz/7NR6s8xu+vFyQxXVl80I22p0TCFfCGUquNHSRFWNllPGnLhjBOT5a1bwKlB5FhoWriObv+1yl+/rhoPOuzWVfeYATHZ3kBLP4qi3sEqA0uXiFzK134KdkRQNS80d+C2ZOnX7t1gE1v+zCQ0xB2NMU1h/5liosZi8TBtEFOouu1KYt5E7nbCWqxcgG0vOgcYTdRs2v9/WVMQWqDL5f574XX8eHVWnf9mShrcDpr9X/6+pYxjDMJkX/Z6Iet+SB26JWNv0/lVjSLp8bJzLpXpN105C0nfemdPO0peTrlN95d2dFHCBakkY1a/mcZfZB6oh1Pl33519oiiJJwHU+RkTREbx2yitp2WH9+CJH4npKvjS8EvGQn6Ih82Zp+eGh8oK7MBsLX7zQOShuj8VKX7roHhPVeEByXGUyZPXwdJG51Zl0iZ9gE3brUt8wOtv59zVXX7koP6mbeDbWmCpdv8NH75+GyeFFquKjTFW20GC1dZDe2LFAVYO+K7XhGPaaSBEk8New4mQIRyxwrFvug1yq6Cwq5XaynddwPgf/FdWJzkHjxd7g/wTPZ1nmGty8jR8LjNjlzlwLQzmijbZ+0fWvQWgSVn8/qdXRywuUXPoTao+/VBBsV/qkPM1KC4LDTfRnlV2GblL5jIoSnunFE4MuMurNZMFRoaa6hixOW9oqUHFaC6z4aktINcK5feza3oID5ZbPic0VtDOYm+Uh5ewBegF8SLcBKbamITwLj7avKQRF43y4x0ca16Q1MvDmVrYnybO/QVqY8PMdg9/5S7Gx1dUPZW1nwCRzsZY698WWYO8Evyuheb0RG1Rz5HtyFvF7kXO6PGvPlO8xGFXfVJR4mugSX7x59h7/OkUZHUNNkYWJq7IEkjz8qYGe8znHcl67zM0pCsKXQtSnFoAJynwKYAjzcLO2LtEUlwmqlD+yJ1Qgc6MhPsc+rSNQgDLM+NiqzqkdSinPloFmLHlYtMqvnzUBd+IhfWfZqk4F4nd/Oe+vOVBru9x2LbuFso8+KPw/nZqTQu2+0tmQNOwWaUaoy5x/y+7ZXuvmgDWfa9Su5aZq37888+nezVq40qkC3+HZuPlGrjetqt13+s3WyEjGplSExf5NZKrKEMgMXje2lYOBpITtcRZAXo1Gol7K2xtsq0nz8HH0EUYl6uiUqfL/MizLOOv1bD1f877J9CYOmORRWaCqILDvSlzkbLA7g0q7X+w7j3f5zfr2BiNAaXrpBL0aTzG6HAN+Bdj8TA3XArQe/Wnh7aVjMHjB+lMBdRJ37KCNLDOa3qrazY8BsvgJ74WwjNWjvFqYGjWHlkY7eI87E2+nmginc850J+S/3wC9zsmrf+zTbYh95lMPqSeGlyE5jMBpvSH9sO/luOfWndhCgdC5AKH1nZWOPdOazdE7oIs2W6ZiTH+9pwpv1iw83XHcresB3NZLpoM1H7HKtHBN09QT8+d5DCSkupp9dY7vI8/pmT0C4kTA3mkoJPk8TFoH9qoLJjusnS/y7F5/E7QE8D5+n5uLrPDG2zuYqcY3j67NBA3oZvKcabPaSsEJQDuTpQ6jOELx+xSJrDeenrwXsdJlHoL/BuSJUw2ePZb5F6iytBkHf+WpqdRNjlMRUPlEW+i7OByd29o11Y/M2FG5eWHTtKviV/bUX59977o2VgEaKaatXszsAepBctsl4YVpqGcsnlJ0DrtWjVtCwsiOx1ustwqWSvK+ODNCbm6+JoELFz8OTg1G0M39zBnSCNmz1X5bnTLGAWZDptR4FRdjr3vbwgiIKMnwtb4hLd9aqCZ0tZ65Q4XBbMhJBWZGfh3tobqzse8wLiQYs2LO1ZCalY4VnqtQ7qBZKLcTpzIkKS0dJRvUkfVScvodRmH9IczIZTB6uln8if3GzLWNT/Nz9FiE78yHIjoPlwNucyggP0c9HFYyXPRWfqa0SEo+/4MOv+dfpqY+2ZCEDxy6RfGGyp6zLX1H4iQIOLSU0S7Wg4I++a/UExQzfAxscWzkCnaGLyNG8wXX0sbPAGdgbI7xaeVgibRi1sqDD8BEmqbR+/uizJTJR7snJy5XCZ3EAB0+6dub6CMYsLFBRDkNHtfWCvFdB4SYtdblUFGkrluc49wDhU+mgBYJtYb89TfPd77Bo7YeC1BJ2ax8/WcBjao0OVx65IHiDDZ89mlXlh5M68Ts4gwe4NjyYNYpdka7rCXP4XbTL3K+6hjt3kBH0/X76EN7BHOfYbQrcNP2MtPBmUj1lFmmAwrC3OQKEknbQRXzZKdCtKHfaILVkFWqlxNlDfvsKj3+QGckWjx8O0Mty2GQMC4h/c+576bzq/sbuaMx2Pdf08W1DrjuXNIjLAEBGEDyYgnJjCtyV44SkX1WYcCCEkc3zRlLERKxPPanzXgL+DkO5qCOOyDvxKqlVhbQvUU16khSmUXpJZYEa6XXlavIvApQxZnv9Vl3x1Tz95J4Wr2dN0CJD0T1wMq6KtaneOpVpFi2Hiw/l0HgS/zz08DLzqFM/IfsCwgTSusxTtxB5R4z3y2aH1anzQiLGsRNFK6a1cJLztKjACrxG3sT0zyoRR/dQUCP0NsEZ++PtT2IWnPJC2R+CkC3OLCXCvtnfSRd0UUG+gz++v82GL4c138zTRTD5GxoZmSq2IHePZgIU/26LSfDhuUaWMPP1+YZ9DnzZtJJgKe+Y8df96wRbZEgZwCrbHB95qU/ZQU1ZBbxthMBcmpmTI+6U0i85sF1Ibn+NlzzPrrfFt/VJnlCo7de7zd2cSn87ucT1AuEvwneFIAPWx1ML7UXufdivJPUiRVg4A8IUTo37arrccYDq0WXDsAMSeeSlwsLF8N80CwEK2H6nSFwarmbwQvfv7Y4bEPC1Q5yMILgsRX9d/a2UMZcBL/KMaqn1GSN8oaLB4sdTU81qElICikoCdz8+or5+UXg+lBGMn1yBtbza+t/PUzQCSl6Qr1pdG2BHYvw+aUMnPAPi7YfzMfK2yiD49XvwgAsmm3kI7Mf54v7iZuo8tHhnafSVTxjx5Dv0/ZgO749g2cmWpqoP9Q3ZTzCYMTmn37bNQn6v8WN+obVDMpuFkHnz1YPP63cGafdEV3ElsC/DYyVmuq1BKDvoANi1yd3XrzQTFJhjgFpvze1cA2uBc8B+P55fYzI1rJWmf93NwXzU+nm4e/YNX2/vkfS42O/IBvLjzs+2x3MWM4YegngADrATBhq8bETkzziKPxAvuDrpvjHV95dveeWGKrnk668TbK6UUCQdTJpao1kvk8NnCs3DsmdZm++PCJgjWDD+kfKlq7Nbvn+6C0RQAKBiDLjw3gEmajHsvvtrLSVZdSmnIbzhWnM79F1HoZX+sIkF8EkwZbS0ROrDQUDRPb/KHtpatFZtIJiJeA1EzfunRsAp+piKhX9qmJG8p96jthth/HsM6wbFJcnhxkZetOsyvjmJb/qYh58DPIu+U+YPF4RXJp0ubIGgCQaFiW45tcgL+VsthD/b5VsBzz7UsJDOj9FqX5ozY0c0/RUH6MA7VAdsbF8bTqU1PH9GBPQCARIEhgygEA4GuR0JULxwfiddhg3FbQusNYZUET0JSAXs07JHMfP1lX3elki1j4TvlHs8Ox80pQhGapUtX668riOt7enCdw9j/QjA35lh68N8Zc1xZp5eX425w7Rd3s0uYXIP3ye6gDBO3djQvdoVODFX9IEy2Q2BH1/CZaNbNBpscJ4BgA20/svhBDu30lG2KiE41kl8k5aCj0bJvMAo7Mta1Dw5iOTtaXv0SNnz9YpKUNoVosEdRo3GVMudLM9QdA5+ZxXiD6kLdyjcm/hyfFlIJSJQ1K6JzDAaYgxTh6ouL4hXUZ7wSW21bQ9Gva0bUn9aJ/S8x8wCbGo5W+a3rwCGwu7mIbtrPL/5po097Jhb5imcSJMBxzgM2IAxPNZ/vUnlncpAd/Rk8cYEBsJ3S/SH+wrSraslx/2dTCVttafGgS7sfVTtrgG15ZnU/WtklAk1T/Es/fY97hIbNgjWJEgkOflbfOSEZyRfyK3iR8oxavOhK+5na5uSYF/7aLwm8Lf2DvE3GKrmk6EOtbR7MYhIbkxZJAWaNKzf8j1b2+z7xfc77RnhkvyjUk9TTr1DRH6IERO+9o7hd1O+0JRHwQbhAKnpnJVHEx+5XYw4JTG4JfQYvYPunVrAXoK6jBSiUI7SOldKqsAXqa75l83W1SrMl58II+iXzqhsGYStkPnOfAVwmhXn3TEtdLvojAtAA0iaGi/GGxB3NpJNWRWSqUgY+hR8hMgv90i173jETjyRn1tgco/kYyoyNiHno9QzbrmLb1It5l6DLwoXiiHfGuFHtqSBSQ5CkVtlmzufCrAW1G9fgQMX4P5GCE2nBnqJm/Oxr3hXEzTSWBKbAHhv9XnMieAwNoe+taF45RKAAzm4x/Tmj5WkbhqBbH0F/nQhEaREPL6bHBiHJ+OdZ/y6zXtDOJAYr55Hvd1xByPrPql/myNpm+/xym3Mte8RowD8mSMef1m4W+nwX/vX3yMJ+7l/9VosoID00FFyBAFBfjFo0dzV7lO5ahCn2YMlSLsE3uxFiJEpiw4ARVmvfDXCh9SxjGX8Tl41tzA/Ze9WHffhzQW6rLeHffGZFSD96TdlB8TL5sSNl3L+A+IDV2AGBaZfylUKktJAMUQiXSN1X/rrP8QKagIA3TAaTKR0kK38Vd1rKMrorc5JqB+X+J1S3pGw1TqiYP/SRp6gP7KYHEgOBo8u3Kn8/Oe/50f2b9Qc9rg1iq4FaCedbQBfPW3riTsxSxelciOagwjqurtxWzkjIB9SJlYYBZipDvLnxWBr+D2TsjrupfXbBZCCuvErFCUiVzEThhrO6xKyJ3Mtxce8IvyjjdmnrQ9xIoAqrBG2ZPRtESCNtUOvIigTALqeab6CDv1W4qC5WIT3Eb59XvLmReczlGvSlNnfbyg/TefdL2UUJsKV+htugRuWTu791bn6U3vXAamYMQJQyNG+Xf6856beLU/JNliLpxUshulblofHpu4tDps7PQ6nEvMS8aHiajobzM2aGTfKBOuzWjP1Ej96Wcu7NzV+9OM9r6eRftAQsP4oqsa6YzxMYYcTKDTvr8h6KAMzVl5qu1OsM4HjnTXnG+eLXqLX/KkbS7rpMtHLWMvzXxQmfgV8v0H3Wv1Pvhmim0c+RVGXv9/BHaHgqSyEjr82Us/MD5Did0gjwEW4lNBlejVb0XnBCYG8jd7/7V9v6volh0RryW6IQzEfvnb4GklaoYons1KORKDLtzgqkPLpRU0Z2RPBTJfALBqszwxJcgT0nKuRMPH5cVzs/E5THRFvKvXwMsZNl9sudzv27m50MDAff6Fz27x3bRncr87cTaFrjlXSTRYFqbIJvrN8Vq4q0Pm3U8FoEE5oML7SW4bWEBG8Az3A/Ig9gvNlrYvgBgGTycIncJgBQ9bDL6QEosDstMcKUT7SI34t/45fxvzbG+7jOL1kr5FTzlQvYyFFDmwkHGMvOd7hwm+XXG1fl3AmO7gGxsoY9YooMs3Qxnij4eykFfYohS+FOD8P3+1juO4BBeSRImWqVing0ay3Wahv4i3XaPcw8RP7WFw5ysniDGcYeaj1Rr/TkwI63XwhH4p/iLpzY79zAGvvJrvOOxCRT512ZyTh8O1HlVOiO2Z3tC3lvWSNM64+h0tNohXOwyj9GuXpuwzNhFxdNXEvmrkf1/qDmGaIVp8zzyDgMqdJnihcItmDtw4ipOO6eE/jMRnTNpPW9/hik8kwL41cM9Ti8s67o54JZ3I9hCWIfyxlXzYSoj3Ryk7keuOPt2qppOnZ1AVBxIhaWn6Psa5Vh1SCJK0ygADh5sNy6wn0YDMO8c4MOK3Yd1z8zmgSnqoWO5CGSZsIa8X5l5HbHTLyu9Eumb2NTXh9m3H5/rNRo05ScsEUPktx6l2Fkp9ZRZ1atORFRf5bnZHGC/W+JJnF3zQvdI7eSxQBJvik0HJUY0ipEtW2qoKjRYXpPx9HQiSIdT6lbgsLUUIs4QI37m8x9dc6RZug5+wPR0EeTbh2zZH5L24usVBUO0UdCQyGe0J52Za0RTttwAG6mgYMEC8GePkKQ9seF6UuzKPi+NMK/YA/BWiAJTCisDbrQnngfVn2spQpJsk1An+f+r6uyDsH8zRjSzCvH5KJTJTy3lc/cucU9UoNGd/4L1OIt4d+FlVAVuxI1Kizqpe3GvX37DF7mUWaKvFu8bsan7lAfcoAEtXrqVnYmXFdYlw4eHgdKJHIjDPiTysQ/7yk6R//+Dfwf1zf9LuWa/lXuyr6/37L1J+7vP41+z+u8zqO478d6H8b5gJEIAgCn7h8nsmWqviv//Euqx9Insn9d/g//v7Xd2zcD32Vxm11x/+80UvL13LI/ka3xTBXa9n9XzQHgzD0z+b+JT/Tf0lhrP+v//FqrP+p9/8/ov6nQcxL/C9LGcP/K2Ar/+Zz3qf531xL/sff/+v/6vaz//zWX286c9wv32Hulv/5+//pmf//Opz3e94OY579y/LXuP/Pff9/1sr//WT/uVjs/2Z4/2z0yf75sv4/Mcf/yhT/GdWL2y3/90AJmJltox1BxYR76VAxoN/+awqX/I//3sX/+PB/Niz4Hy37H1wY/L/w4f/kYn8AGXWnZAlybKhFkk8gG8ixJRDBYsMWrRwn1umEDSfI8ku0G+aLkgB1A5isNz0xa9zXlwQUHB6S/kW2yhXda/g1/Q2O3dBxdxVX4yOAX1Cp8ZHS1//c+WcSRY2xc9l1F/Ft2pLHU7S/oFLWCSoUL1kiPCpcwamg93WtNkI34IFonE15qJDU0As5gRSZae1mGyxhUcGL+gqaAZChH77gWarHqusxI89iHGxz6qiL0AZFPShWOuw6p8KzkOuE126ulzpBwG5QvVge0iM0yVmGHOPbBwqihwgRC9+Vi1kJzeUL6wbErgY5zXsjJbihkFeIZbvVnEsQ1/vAwoRxf+8lNoZ34DTV5/jHP/6HDf7DXP8nO6j59b9OMM8Xetzl/87q/3j/7is0vt/n0/z//pvxj79u43PyR9UP7VBU+fLf2/rrtf/sBf+plf/cp3//L//217WG//5f/ndaRwCyQlEAAA=="
  }
}
///
resource "kubernetes_config_map" "cm_variables" {
  metadata {
    name = "env"
    annotations = {
      description = "Configuration Map for environment variables for Container Gateway"
    }
  }
  data = {
    "extra-java-args.env"           = "-Dcom.l7tech.bootstrap.env.license.enable=true -Dcom.l7tech.bootstrap.autoTrustSslKey=trustAnchor,TrustedFor.SSL,TrustedFor.SAML_ISSUER"
    "ssg-database-jdbc-url.env"     = "jdbc:mysql://${var.db_instance_endpoint}/dbCAGateway"  # "jdbc:mysql://${aws_db_instance.db_instance.endpoint}/dbCAGateway"
    "ssg-cluster-host.env"          = substr("${aws_eks_cluster.tf_eks.endpoint}",8,100)
    "ssg-cluster-password.env"      = "password123"
    "ssg-jvm-heap.env"              = "4g"
  }
  depends_on = ["aws_eks_cluster.tf_eks"]
}

#########################################
#                Secrets                #
#########################################

resource "kubernetes_secret" "gw_secret" {
  metadata {
    name = "gw-secret"
    labels = {
      name = "gw-secret"
    }
    annotations = {
      description = "Template for Secrets for the Gateway"
    }
  }
  data = {
    "ssg.adminusername" = "YWRtaW4="
    "ssg.adminpassword" = "cGFzc3dvcmQ="
  }
}
///

resource "kubernetes_secret" "gw_db_secret" {
  metadata {
    name = "gw-db-secret"
    labels = {
      name = "gw-db-secret"
    }
    annotations = {
      description = "Template for SSG Database Secrets for the Gateway"
    }
  }
  data = {
    "ssg.databaseuser" = "${var.rds_username}"
    "ssg.databasepass" = "${var.rds_password}"
  }
}

///
resource "kubernetes_secret" "mysql_secret" {
  metadata {
    name = "mysql-secret"
    labels = {
      name = "mysql-secret"
    }
    annotations = {
      description = "Template for Database Secrets for the Gateway"
    }
  }
  data = {
    "mysql.user" = "ZGJDQUdhdGV3YXk="
    "mysql.password" = "ZGJDQUdhdGV3YXkyMDE5"
  }
}
*/