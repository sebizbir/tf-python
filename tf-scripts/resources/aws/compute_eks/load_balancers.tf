###################################################################################
#                               CA Load Balancer                                  #
###################################################################################
resource "aws_elb" "generic_eks_load_balancer" {
  name               		= "${var.eks_load_balancer_generic_name}"

  listener {
    instance_port     		= 80
    instance_protocol 		= "tcp"
    lb_port           		= 80
    lb_protocol       		= "tcp"
  }

  health_check {
    healthy_threshold   = 10
    unhealthy_threshold = 2
    timeout             = 5
    target              = "TCP:8080"
    interval            = 30
  }

  cross_zone_load_balancing   = true
  security_groups             = ["${var.sg_eks_load_balancer}"]
  subnets                     = ["${var.private_subnet_cidr_az1_internal_app}", "${var.private_subnet_cidr_az2_internal_app}"]
  internal                    = true

  tags 						= {
    Name 					= "${var.eks_load_balancer_generic_name}"
    "kubernetes.io/cluster/${var.generic_eks_cluster_name}" = "owned",
  }
}
