###########################################################################################
# EKS currently documents this required userdata for EKS worker nodes to                  #
# properly configure Kubernetes applications on the EC2 instance.                         #
# We utilize a Terraform local here to simplify Base64 encode this                        #
# information and write it into the AutoScaling Launch Configuration.                     #
# More information: https://docs.aws.amazon.com/eks/latest/userguide/launch-workers.html  #
###########################################################################################
locals {
  tf_eks_node_userdata        = <<USERDATA
#!/bin/bash
set -o xtrace
/etc/eks/bootstrap.sh --apiserver-endpoint '${aws_eks_cluster.generic_eks.endpoint}' --b64-cluster-ca '${aws_eks_cluster.generic_eks.certificate_authority.0.data}' '${var.generic_eks_cluster_name}'
USERDATA
}
################################################################################
#                             Launch configuration                             #
################################################################################

resource "aws_launch_configuration" "generic_eks_lc" {
  associate_public_ip_address = false
  iam_instance_profile        = "${var.aws_iam_instance_profile_node_name}"
  image_id                    = "${var.generic_eks_image_id}"
  instance_type               = "${var.generic_eks_node_instance_type}"
  name_prefix                 = "${var.generic_eks_node_name}"
  security_groups             = ["${var.sg_generic_eks_node}"]
  #user_data_base64            = "${base64encode(local.generic_eks_node_userdata)}"
  key_name                    = "${var.generic_eks_key_name}"

  lifecycle {
    create_before_destroy = true
  }
}
################################################################################
#                             Autoscaling group                                #
################################################################################
resource "aws_autoscaling_group" "generic_eks_as" {
  desired_capacity            = "1"
  max_size                    = "1"
  min_size                    = "1"
  launch_configuration        = "${aws_launch_configuration.generic_eks_lc.id}"
  name                        = "${var.generic_eks_node_name}"
  vpc_zone_identifier         = ["${var.private_subnet_cidr_az1_internal_app}", "${var.private_subnet_cidr_az2_internal_app}"]
  load_balancers       	      = ["${aws_elb.generic_eks_load_balancer.name}"]

  tag {
    key                       = "Name"
    value                     = "${var.generic_eks_node_name}"
    propagate_at_launch       = true
  }

  tag {
    key                       = "kubernetes.io/cluster/${var.generic_eks_cluster_name}"
    value                     = "owned"
    propagate_at_launch       = true
  }
}