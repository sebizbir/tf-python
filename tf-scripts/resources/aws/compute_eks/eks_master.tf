##################################################################
#                 Master EKS Cluster                             #
##################################################################
resource "aws_eks_cluster" "generic_eks" {
  name                      = "${var.generic_eks_cluster_name}"
  role_arn                  = "${var.generic_eks_master_role_arn}"

  vpc_config {
    security_group_ids      = ["${var.sg_generic_eks_master}"]
    subnet_ids              = ["${var.private_subnet_cidr_az1_internal_app}", "${var.private_subnet_cidr_az2_internal_app}"]
    endpoint_private_access	= "${var.generic_eks_endpoint_private_access}"
    endpoint_public_access  = "${var.generic_eks_endpoint_public_access}"
  }
}