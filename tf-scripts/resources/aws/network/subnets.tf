##########################################################################
#                  Public Subnet - External Subnet - AZ1                 #
##########################################################################
resource "aws_subnet" "public_subnet_cidr_az1" {
  vpc_id 					= "${aws_vpc.vpc_id.id}"

  cidr_block 				= "${var.public_subnet_cidr_az1}"
  availability_zone 		= "eu-west-2a"
  map_public_ip_on_launch   = true

  tags 					= {
    Name 				= "External Subnet - AZ1"
  }
}

resource "aws_route_table_association" "public_subnet_cidr_az1" {
  subnet_id 				= "${aws_subnet.public_subnet_cidr_az1.id}"
  route_table_id 			= "${aws_route_table.public_route.id}"
}

##########################################################################
#                  Public Subnet - External Subnet - AZ2                 #
##########################################################################
resource "aws_subnet" "public_subnet_cidr_az2" {
  vpc_id 					= "${aws_vpc.vpc_id.id}"

  cidr_block 				= "${var.public_subnet_cidr_az2}"
  availability_zone 		= "eu-west-2b"
  map_public_ip_on_launch   = true

  tags 					= {
    Name 				= "External Subnet - AZ2"
  }
}

resource "aws_route_table_association" "public_subnet_cidr_az2" {
  subnet_id 				= "${aws_subnet.public_subnet_cidr_az2.id}"
  route_table_id 			= "${aws_route_table.public_route.id}"
}
###################################################################################
#                  Private Subnet - Internal Data Subnet - AZ1                    #
###################################################################################
resource "aws_subnet" "private_subnet_cidr_az1_internal_data" {
  vpc_id 					= "${aws_vpc.vpc_id.id}"

  cidr_block 				= "${var.private_subnet_cidr_az1_internal_data}"
  availability_zone 		= "eu-west-2a"

  tags 					= {
    Name 				= "Internal Data Subnet - AZ1"
  }
}

resource "aws_route_table_association" "private_subnet_cidr_az1_internal_data" {
  subnet_id 				= "${aws_subnet.private_subnet_cidr_az1_internal_data.id}"
  route_table_id 			= "${aws_route_table.private_route.id}"
}

###################################################################################
#                  Private Subnet - Internal Data Subnet - AZ2                    #
###################################################################################
resource "aws_subnet" "private_subnet_cidr_az2_internal_data" {
  vpc_id 					= "${aws_vpc.vpc_id.id}"

  cidr_block 				= "${var.private_subnet_cidr_az2_internal_data}"
  availability_zone 		= "eu-west-2b"

  tags 					= {
    Name 				= "Internal Data Subnet - AZ2"
  }
}

resource "aws_route_table_association" "private_subnet_cidr_az2_internal_data" {
  subnet_id 				= "${aws_subnet.private_subnet_cidr_az2_internal_data.id}"
  route_table_id 			= "${aws_route_table.private_route.id}"
}
###################################################################################
#                  Private Subnet - Internal App Subnet - AZ1                     #
###################################################################################
resource "aws_subnet" "private_subnet_cidr_az1_internal_app" {
  vpc_id 					= "${aws_vpc.vpc_id.id}"

  cidr_block 				= "${var.private_subnet_cidr_az1_internal_app}"
  availability_zone 		= "eu-west-2a"

  tags 					= {
    Name 				= "Internal App Subnet - AZ1"
  }
}

resource "aws_route_table_association" "private_subnet_cidr_az1_internal_app" {
  subnet_id 				= "${aws_subnet.private_subnet_cidr_az1_internal_app.id}"
  route_table_id 			= "${aws_route_table.private_route.id}"
}

###################################################################################
#                  Private Subnet - Internal App Subnet - AZ2                     #
###################################################################################
resource "aws_subnet" "private_subnet_cidr_az2_internal_app" {
  vpc_id 					= "${aws_vpc.vpc_id.id}"

  cidr_block 				= "${var.private_subnet_cidr_az2_internal_app}"
  availability_zone 		= "eu-west-2b"

  tags 					= {
    Name 				= "Internal App Subnet - AZ2"
  }
}

resource "aws_route_table_association" "private_subnet_cidr_az2_internal_app" {
  subnet_id 				= "${aws_subnet.private_subnet_cidr_az2_internal_app.id}"
  route_table_id 			= "${aws_route_table.private_route.id}"
}