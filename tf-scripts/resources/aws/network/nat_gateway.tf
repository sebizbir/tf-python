#########################################
#                   EIP                 #
#########################################
resource "aws_eip" "nat_gw_eip" {
  vpc      				= true
  tags 					= {
    Name 				= "NAT Gateway IP - ${var.env_id}"
  }
}
#########################################
#           NAT Gateway                 #
#########################################
resource "aws_nat_gateway" "nat_gw" {
  allocation_id 			= "${aws_eip.nat_gw_eip.id}"
  subnet_id     			= "${aws_subnet.public_subnet_cidr_az1.id}"
  depends_on 				= ["aws_internet_gateway.igw"]
  tags 					= {
    Name 				= "NAT Gateway - ${var.env_id}"
  }
}
