#################################################################################
#           VPC, Internet Gateway, Elastic IP, NAT Gateway                      #
#################################################################################
#########################################
#                   VPC                 #
#########################################
resource "aws_vpc" "vpc_id" {
  cidr_block 			= "${var.vpc_cidr}"
  enable_dns_hostnames 	= "${var.vpc_dns}"
  tags 					= {
    Name 				= "${var.env_id}"
  }
}

#########################################
#                   IG                 #
#########################################
resource "aws_internet_gateway" "igw" {
  vpc_id 				= "${aws_vpc.vpc_id.id}"
  tags 					= {
    Name 				= "${var.env_id} Internet Gateway"
  }
}
