##################################################################
#                     ECS Generic ALB                            #
##################################################################
resource "aws_lb" "ecs_load_balancer_generic" {
  name               		  = "${var.ecs_load_balancer_generic_name}"
  internal                    = false
  load_balancer_type          = "application"
  security_groups             = ["${var.sg_ecs_load_balancer}"]
  subnets                     = ["${var.private_subnet_cidr_az1_internal_data}", "${var.private_subnet_cidr_az2_internal_data}"]
  tags = {
    Name        = "${var.ecs_load_balancer_generic_name}"
    Environment = "${var.env_id}"
  }
}

##################################################################
#                     ECS Generic TG                             #
##################################################################
resource "aws_lb_target_group" "ecs_load_balancer_tg_generic_main" {
  name     = "${var.ecs_load_balancer_tg_generic_main_name}"
  port     = 80
  protocol = "HTTP"
  vpc_id   = "${var.vpc_id}"
}

resource "aws_lb_listener" "ecs_generic" {
  load_balancer_arn = "${aws_lb.ecs_load_balancer_generic.id}"
  port              = "80"
  protocol          = "HTTP"

  default_action {
    target_group_arn = "${aws_lb_target_group.ecs_load_balancer_tg_generic_main.id}"
    type             = "forward"
  }
}
