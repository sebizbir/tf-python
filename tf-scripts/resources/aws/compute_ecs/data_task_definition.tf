data "template_file" "task_definition" {
  template = "${file("task-definitions/service.json")}"

  vars {
    image_url        = "${var.ecr_generic_name}:latest"
    container_name   = ".."
   # log_group_region = "${var.aws_region}"
   # log_group_name   = "${aws_cloudwatch_log_group.generic.name}"
  }
}