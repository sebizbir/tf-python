##################################################################
#                     Generic Service                            #
##################################################################
resource "aws_ecs_service" "generic_main" {
  name = "${var.ecs_service_name_generic}"
  cluster = "${aws_ecs_cluster.main_ecs_cluster.id}"
  task_definition = "${aws_ecs_task_definition.service.arn}"
  desired_count = "${var.ecs_service_desired_count_generic}"
  iam_role = "${var.iam_role_ecs_service}"

  load_balancer {
    target_group_arn = "${aws_lb_target_group.ecs_load_balancer_tg_generic_main.arn}"
    container_name = "mongo"
    container_port = 8080
  }
}

