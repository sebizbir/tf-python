resource "aws_sns_topic" "generic_sns_topic" {
  name = "${var.generic_sns_topic_name}"
  #kms_master_key_id = "alias/aws/sns"
  tags 				= {
    Name 			= "${var.generic_sns_topic_name}"
  }
}