resource "aws_sns_topic_subscription" "generic_sns_subscription" {
  topic_arn = "${aws_sns_topic.generic_sns_topic.arn}"
  protocol  = "application"
  endpoint  = ""
}