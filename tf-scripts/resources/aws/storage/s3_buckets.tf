resource "aws_s3_bucket" "statefile_bucket" {
  bucket = "${var.statefile_bucket_name}"
  region = "eu-west-2"
}

resource "aws_iam_role" "generic_bucket_service_role" {
  name = "generic-bucket-service-role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
  tags = {
    Name = "generic-bucket-service-role"
  }
}