output "sg_ec2_app" {
  value = aws_security_group.sg_ec2_app.id
}
output "sg_ecs_load_balancer" {
  value = aws_security_group.sg_ecs_load_balancer.id
}
output "sg_ecs_load_balancer_name" {
  value = aws_security_group.sg_ecs_load_balancer.name
}
output "sg_eks_load_balancer" {
  value = aws_security_group.sg_eks_load_balancer.id
}
output "sg_eks_load_balancer_name" {
  value = aws_security_group.sg_eks_load_balancer.name
}
output "sg_rds_mssql_database" {
  value = aws_security_group.sg_rds_mssql_database.id
}
output "sg_rds_mssql_database_name" {
  value = aws_security_group.sg_rds_mssql_database.name
}
output "sg_rds_mysql_database" {
  value = aws_security_group.sg_rds_mysql_database.id
}
output "sg_rds_mysql_database_name" {
  value = aws_security_group.sg_rds_mysql_database.name
}
output "sg_generic_eks_master" {
  value = aws_security_group.sg_generic_eks_master.id
}
output "sg_generic_eks_node" {
  value = aws_security_group.sg_generic_eks_node.id
}