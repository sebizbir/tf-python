variable "vpc_id" {}
variable "sg_ecs_load_balancer_name" {}
variable "sg_eks_load_balancer_name" {}
variable "sg_rds_mssql_database_name" {}
variable "sg_rds_mysql_database_name" {}