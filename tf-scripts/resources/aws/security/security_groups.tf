#########################################################################################################
#                                           Security groups                                             #
#########################################################################################################
##################################################################
#                       SG - EC2 App instances                   #
##################################################################
resource "aws_security_group" "sg_ec2_app" {
  name 				= "sgEC2App"

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }
  vpc_id 				= "${var.vpc_id}"

  tags 				= {
    Name 			= "sgEC2App"
    Description 	= "Used to secure Application instances"
  }
}

resource "aws_security_group_rule" "sg_ec2_app_1" {
  description              = "Internet - http- Application instances"
  from_port                = 80
  protocol                 = "tcp"
  security_group_id        = "${aws_security_group.sg_ec2_app.id}"
  cidr_blocks              = ["0.0.0.0/0"]
  to_port                  = 80
  type                     = "ingress"
}
##################################################################
#                       SG - ECS Load Balancer                   #
##################################################################
resource "aws_security_group" "sg_ecs_load_balancer" {
  name 				= "${var.sg_ecs_load_balancer_name}"

  ingress {
    from_port 		= 80
    to_port 		= 80
    protocol 		= "tcp"
    cidr_blocks 	= ["0.0.0.0/0"]
  }
  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }
  vpc_id 			= "${var.vpc_id}"

  tags 				= {
    Name 			= "${var.sg_ecs_load_balancer_name}"
    Description 	= "Used to secure the ECS Elastic Load Balancer"
  }
}
##################################################################
#                       SG - EKS Load Balancer                   #
##################################################################
resource "aws_security_group" "sg_eks_load_balancer" {
  name 				= "${var.sg_eks_load_balancer_name}"

  ingress {
    from_port 		= 80
    to_port 		= 80
    protocol 		= "tcp"
    cidr_blocks 	= ["0.0.0.0/0"]
  }
  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }
  vpc_id 			= "${var.vpc_id}"

  tags 				= {
    Name 			= "${var.sg_eks_load_balancer_name}"
    Description 	= "Used to secure the EKS Elastic Load Balancer"
  }
}
##################################################################
#                       SG - MSSQL RDS database                  #
##################################################################
resource "aws_security_group" "sg_rds_mssql_database" {
  name 				= "${var.sg_rds_mssql_database_name}"

 # ingress {
 #   from_port 		= 80
 #   to_port 		= 80
 #   protocol 		= "tcp"
 #   cidr_blocks 	= ["0.0.0.0/0"]
 # }
  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }
  vpc_id 			= "${var.vpc_id}"

  tags 				= {
    Name 			= "${var.sg_rds_mssql_database_name}"
    Description 	= "Used to secure the MSSQL RDS Database"
  }
}
##################################################################
#                       SG - MySQL RDS database                  #
##################################################################
resource "aws_security_group" "sg_rds_mysql_database" {
  name 				= "${var.sg_rds_mysql_database_name}"

  # ingress {
  #   from_port 		= 80
  #   to_port 		= 80
  #   protocol 		= "tcp"
  #   cidr_blocks 	= ["0.0.0.0/0"]
  # }
  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }
  vpc_id 			= "${var.vpc_id}"

  tags 				= {
    Name 			= "${var.sg_rds_mysql_database_name}"
    Description 	= "Used to secure the MySQL RDS Database"
  }
}
##################################################################
#                 SG - Master EKS Cluster                        #
##################################################################
resource "aws_security_group" "sg_generic_eks_master" {
  name        = "Generic-EKS-master"
  description = "Cluster communication with worker nodes"
  vpc_id      = "${var.vpc_id}"

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "Generic-EKS-master"
    Description = "Used to secure the Generic-EKS - Master"
  }
}
##################################################################
#                 SG - Nodes EKS Cluster                         #
##################################################################
resource "aws_security_group" "sg_generic_eks_node" {
  name        = "Generic-EKS-nodes"
  description = "Security group for all nodes in the cluster"
  vpc_id      = "${var.vpc_id}"

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "Generic-EKS-nodes"
    Description = "Used to secure the Generic-EKS - Nodes"
  }
}