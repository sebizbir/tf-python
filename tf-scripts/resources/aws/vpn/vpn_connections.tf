#########################################
#   Eastnets VPN                        #
#########################################
resource "aws_customer_gateway" "cg_generic" {
    bgp_asn                           = 65000
    ip_address                        = ""
    type                              = "ipsec.1"
    tags 								= {
        Name 							= "Generic"
    }
}
#########################################
#   VPN + VPG                           #
#########################################
resource "aws_vpn_gateway" "generic_vpn_gw" {
    vpc_id                            = "${var.vpc_id}"
    tags                              = {
        Name                            = "Generic VPG"
    }
}
