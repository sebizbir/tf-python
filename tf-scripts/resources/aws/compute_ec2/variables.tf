# Null resource
#variable "generic_ad" {}
# Launch configuration
variable "ec2_app_image_id" {}
variable "ec2_app_instance_type" {}
variable "ec2_app_key_name" {}
variable "sg_ec2_app" {}
# Subnets
variable "private_subnet_cidr_az1_internal_app" {}
variable "private_subnet_cidr_az2_internal_app" {}