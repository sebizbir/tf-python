/*
resource "null_resource" "active_directory_found" {
	triggers    = {
		ad_name  = "${var.generic_ad}"
	}
}
#################################################################################
#                             Domain join role                                  #
#################################################################################
resource "aws_iam_role" "ec2_domain_join" {
	name                  = "EC2DomainJoin"
	assume_role_policy    = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
POLICY
}

resource "aws_iam_role_policy_attachment" "ec2_domain_join_attach_1" {
	policy_arn            = "arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore"
	role                  = "${aws_iam_role.ec2_domain_join.name}"
}
resource "aws_iam_role_policy_attachment" "ec2_domain_join_attach_2" {
	policy_arn            = "arn:aws:iam::aws:policy/AmazonSSMDirectoryServiceAccess"
	role                  = "${aws_iam_role.ec2_domain_join.name}"
}

resource "aws_iam_instance_profile" "ec2_domain_join_profile" {
	name                  = "ec2_domain_join_profile"
	role                  = "${aws_iam_role.ec2_domain_join.name}"
}
*/
#################################################################################
#                             Application Instances                             #
#################################################################################
resource "aws_launch_configuration" "ec2_app_lc" {
	name_prefix 		      		= "App"
	image_id 				        = "${var.ec2_app_image_id}"
	instance_type 		        	= "${var.ec2_app_instance_type}"
	key_name                  		= "${var.ec2_app_key_name}"
	#iam_instance_profile      		= "${aws_iam_instance_profile.ec2_domain_join_profile.id}"
	root_block_device {
		volume_type             = "gp2"
		volume_size             = 50
		delete_on_termination   = true
	}

	ebs_block_device {
		device_name = "/dev/sdg"
		volume_type = "gp2"
		volume_size = 100
		encrypted   = true
		delete_on_termination = false
	}
	security_groups 			= ["${var.sg_ec2_app}"]
	/*
	depends_on                = ["null_resource.active_directory_found","aws_iam_role.ec2_domain_join"]
	user_data = <<HEREDOC
  	<powershell>
 	Invoke-WebRequest https://s3.amazonaws.com/ec2-downloads-windows/SSMAgent/latest/windows_amd64/AmazonSSMAgentSetup.exe -OutFile $env:USERPROFILE\Desktop\SSMAgent_latest.exe
 	Restart-Service AmazonSSMAgent
 	Rename-Computer -NewName App
 	Restart-Computer
 	</powershell>
 	HEREDOC
 	*/
}

resource "aws_autoscaling_group" "ec2_app_ag" {
	name_prefix                 		= "Application"
	max_size             				= 1
	min_size             				= 1
	launch_configuration 				= "${aws_launch_configuration.ec2_app_lc.name}"
	vpc_zone_identifier  				= ["${var.private_subnet_cidr_az1_internal_app}", "${var.private_subnet_cidr_az2_internal_app}"]
	tag {
		key                 	= "Name"
		value               	= "Application"
		propagate_at_launch 	= true
	}
	#depends_on = ["null_resource.active_directory_found","aws_iam_role.ec2_domain_join"]
}