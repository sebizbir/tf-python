#################################################################################
#                                   AD Service                                  #
#################################################################################
resource "aws_directory_service_directory" "generic_ad" {
  name     = "${var.generic_ad_name}"
  password = "${var.generic_ad_password}"
  edition  = "${var.generic_ad_edition}"
  type     = "${var.generic_ad_type}"
  short_name  = "${var.generic_ad_short_name}"

  vpc_settings {
    vpc_id     = "${var.vpc_id}"
    subnet_ids = ["${var.private_subnet_cidr_az1_internal_data}", "${var.private_subnet_cidr_az2_internal_data}"]
  }

  tags = {
    Name = "${var.generic_ad_name}"
  }
}
