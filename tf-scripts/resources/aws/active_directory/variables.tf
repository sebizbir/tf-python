variable "vpc_id" {}
variable "private_subnet_cidr_az1_internal_data" {}
variable "private_subnet_cidr_az2_internal_data" {}
variable "generic_ad_name" {}
variable "generic_ad_short_name" {}
variable "generic_ad_password" {}
variable "generic_ad_edition" {}
variable "generic_ad_type" {}