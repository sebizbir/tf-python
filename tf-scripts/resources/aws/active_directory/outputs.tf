output "generic_ad" {
  value = aws_directory_service_directory.generic_ad.id
}
output "generic_ad_url" {
  value = aws_directory_service_directory.generic_ad.access_url
}
output "generic_ad_addresses_first" {
  value = tolist(aws_directory_service_directory.generic_ad.dns_ip_addresses)[0]
}
output "generic_ad_addresses_second" {
  value = tolist(aws_directory_service_directory.generic_ad.dns_ip_addresses)[1]
}
output "generic_ad_sg" {
  value = aws_directory_service_directory.generic_ad.security_group_id
}
