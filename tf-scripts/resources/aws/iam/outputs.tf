output "iam_role_ecs_service" {
  value = aws_iam_role.ecs_service.id
}
output "generic_eks_master_role" {
  value = aws_iam_role.generic_eks_master_role.id
}
output "generic_eks_master_role_arn" {
  value = aws_iam_role.generic_eks_master_role.arn
}
output "generic_eks_node_role" {
  value = aws_iam_role.generic_eks_node_role.id
}
output "generic_eks_node_role_arn" {
  value = aws_iam_role.generic_eks_node_role.arn
}
output "aws_iam_instance_profile_node_name" {
  value = aws_iam_instance_profile.aws_iam_instance_profile_node.name
}