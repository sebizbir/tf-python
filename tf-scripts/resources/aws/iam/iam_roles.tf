#########################################################################################################
#                                                ECS                                                    #
#########################################################################################################
resource "aws_iam_role" "ecs_service" {
  name = "ecsServiceRole"

  assume_role_policy = <<EOF
{
  "Version": "2008-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Service": "ecs.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy" "ecs_service" {
  name = "ecs_service_iam_policy"
  role = "${aws_iam_role.ecs_service.name}"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "ec2:Describe*",
        "elasticloadbalancing:DeregisterInstancesFromLoadBalancer",
        "elasticloadbalancing:DeregisterTargets",
        "elasticloadbalancing:Describe*",
        "elasticloadbalancing:RegisterInstancesWithLoadBalancer",
        "elasticloadbalancing:RegisterTargets"
      ],
      "Resource": "*"
    }
  ]
}
EOF
}
#########################################################################################################
#                                                EKS                                                    #
#########################################################################################################
#########################################################################################################
#                       Setup for IAM role needed to setup an EKS cluster                               #
#########################################################################################################
resource "aws_iam_role" "generic_eks_master_role" {
  name                  = "eksServiceRole-master"
  assume_role_policy    = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "eks.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
POLICY
}

resource "aws_iam_role_policy_attachment" "generic_cluster_AmazonEKSClusterPolicy" {
  policy_arn            = "arn:aws:iam::aws:policy/AmazonEKSClusterPolicy"
  role                  = "${aws_iam_role.generic_eks_master_role.name}"
}

resource "aws_iam_role_policy_attachment" "generic_cluster_AmazonEKSServicePolicy" {
  policy_arn            = "arn:aws:iam::aws:policy/AmazonEKSServicePolicy"
  role		              = "${aws_iam_role.generic_eks_master_role.name}"
}
#########################################################################################################
#                       Setup IAM role & instance profile for worker nodes                              #
#########################################################################################################

resource "aws_iam_role" "generic_eks_node_role" {
  name                  = "eksServiceRole-node"
  assume_role_policy    = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
POLICY
}

resource "aws_iam_role_policy_attachment" "generic_eks_node_AmazonEKSWorkerNodePolicy" {
  policy_arn            = "arn:aws:iam::aws:policy/AmazonEKSWorkerNodePolicy"
  role                  = "${aws_iam_role.generic_eks_node_role.name}"
}

resource "aws_iam_role_policy_attachment" "generic_eks_node_AmazonEKS_CNI_Policy" {
  policy_arn            = "arn:aws:iam::aws:policy/AmazonEKS_CNI_Policy"
  role                  = "${aws_iam_role.generic_eks_node_role.name}"
}

resource "aws_iam_role_policy_attachment" "generic_eks_node_AmazonEC2ContainerRegistryReadOnly" {
  policy_arn            = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
  role                  = "${aws_iam_role.generic_eks_node_role.name}"
}

resource "aws_iam_instance_profile" "aws_iam_instance_profile_node" {
  name                  = "terraform-eks-node"
  role                  = "${aws_iam_role.generic_eks_node_role.name}"
}
