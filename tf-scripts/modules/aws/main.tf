#########################################################################
#                     Network - VPC, CIDR's, Subnets                    #
#########################################################################
module "network" {
  source                                      = "../../resources/aws/network/"
  # VPC data
  env_id                                      = "${var.env_id}"
  vpc_dns                                     = "${var.vpc_dns}"
  vpc_cidr                                    = "${var.vpc_cidr}"
  # Subnets
  public_subnet_cidr_az1                      = "${var.public_subnet_cidr_az1}"
  public_subnet_cidr_az2                      = "${var.public_subnet_cidr_az2}"
  private_subnet_cidr_az1_internal_data       = "${var.private_subnet_cidr_az1_internal_data}"
  private_subnet_cidr_az2_internal_data       = "${var.private_subnet_cidr_az2_internal_data}"
  private_subnet_cidr_az1_internal_app        = "${var.private_subnet_cidr_az1_internal_app}"
  private_subnet_cidr_az2_internal_app        = "${var.private_subnet_cidr_az2_internal_app}"
}
#########################################################################
#                             IAM  - IAM roles                          #
#########################################################################
module "iam" {
  source                                      = "../../resources/aws/iam/"
}
#########################################################################
#                       Security - Security groups                      #
#########################################################################
module "security" {
  source                                      = "../../resources/aws/security/"
  # VPC data
  vpc_id                                      = "${module.network.vpc_id}"
  # Security groups
  sg_ecs_load_balancer_name                   = "${var.sg_ecs_load_balancer_name}"
  sg_eks_load_balancer_name                   = "${var.sg_eks_load_balancer_name}"
  sg_rds_mssql_database_name                  = "${var.sg_rds_mssql_database_name}"
  sg_rds_mysql_database_name                  = "${var.sg_rds_mysql_database_name}"
}
#########################################################################
#                        Active Directory - AD data                     #
#########################################################################
module "active_directory" {
  source                                      = "../../resources/aws/active_directory/"
  # AD data
  generic_ad_edition                          = "${var.generic_ad_edition}"
  generic_ad_name                             = "${var.generic_ad_name}"
  generic_ad_password                         = "${var.generic_ad_password}"
  generic_ad_short_name                       = "${var.generic_ad_short_name}"
  generic_ad_type                             = "${var.generic_ad_type}"
  # VPC data
  vpc_id                                      = "${module.network.vpc_id}"
  # Subnets
  private_subnet_cidr_az1_internal_data            = "${module.network.private_subnet_cidr_az1_internal_data}"
  private_subnet_cidr_az2_internal_data            = "${module.network.private_subnet_cidr_az2_internal_data}"
}
#########################################################################
#                        Repositories - ECR                             #
#########################################################################
module "repositories" {
  source                                      = "../../resources/aws/repositories/"
  # VPC data
  env_id                                      = "${var.env_id}"
  # ECR data
  ecr_generic_name                            = "${var.ecr_generic_name}"
  ecr_generic_mutability                      = "${var.ecr_generic_mutability}"
}
#########################################################################
#                      Data - Databases (MSSQL, MySQL)                  #
#########################################################################
module "data" {
  source                                      = "../../resources/aws/data/"
  # Subnets
  private_subnet_cidr_az1_internal_data            = "${module.network.private_subnet_cidr_az1_internal_data}"
  private_subnet_cidr_az2_internal_data            = "${module.network.private_subnet_cidr_az2_internal_data}"
  # Security groups
  sg_rds_mssql_database                       = "${module.security.sg_rds_mssql_database}"
  sg_rds_mysql_database                       = "${module.security.sg_rds_mysql_database}"
  # MSSQL RDS data
  rds_mssql_identifier                        = "${var.rds_mssql_identifier}"
  rds_mssql_allocated_storage                 = "${var.rds_mssql_allocated_storage}"
  rds_mssql_backup_retention_period           = "${var.rds_mssql_backup_retention_period}"
  rds_mssql_backup_window                     = "${var.rds_mssql_backup_window}"
  rds_mssql_maintenance_window                = "${var.rds_mssql_maintenance_window}"
  rds_mssql_multi_az                          = "${var.rds_mssql_multi_az}"
  rds_mssql_storage_encrypted                 = "${var.rds_mssql_storage_encrypted}"
  rds_mssql_publicly_accessible               = "${var.rds_mssql_publicly_accessible}"
  rds_mssql_storage_type                      = "${var.rds_mssql_storage_type}"
  rds_mssql_engine                            = "${var.rds_mssql_engine}"
  rds_mssql_engine_version                    = "${var.rds_mssql_engine_version}"
  rds_mssql_license_model                     = "${var.rds_mssql_license_model}"
  rds_mssql_instance_class                    = "${var.rds_mssql_instance_class}"
  rds_mssql_username                          = "${var.rds_mssql_username}"
  rds_mssql_password                          = "${var.rds_mssql_password}"
  # Mysql RDS data
  rds_mysql_db_name                           = "${var.rds_mysql_db_name}"
  rds_mysql_family_name                       = "${var.rds_mysql_family_name}"
  rds_mysql_identifier                        = "${var.rds_mysql_identifier}"
  rds_mysql_allocated_storage                 = "${var.rds_mysql_allocated_storage}"
  rds_mysql_backup_retention_period           = "${var.rds_mysql_backup_retention_period}"
  rds_mysql_backup_window                     = "${var.rds_mysql_backup_window}"
  rds_mysql_maintenance_window                = "${var.rds_mysql_maintenance_window}"
  rds_mysql_multi_az                          = "${var.rds_mysql_multi_az}"
  rds_mysql_storage_encrypted                 = "${var.rds_mysql_storage_encrypted}"
  rds_mysql_publicly_accessible               = "${var.rds_mysql_publicly_accessible}"
  rds_mysql_storage_type                      = "${var.rds_mysql_storage_type}"
  rds_mysql_engine                            = "${var.rds_mysql_engine}"
  rds_mysql_engine_version                    = "${var.rds_mysql_engine_version}"
  rds_mysql_instance_class                    = "${var.rds_mysql_instance_class}"
  rds_mysql_username                          = "${var.rds_mysql_username}"
  rds_mysql_password                          = "${var.rds_mysql_password}"
}
#########################################################################
#               Compute EC2 - EC2 image, instance type, keys            #
#########################################################################
module "compute_ec2" {
  source                                      = "../../resources/aws/compute_ec2/"
  # EC2 data
  ec2_app_image_id                            = "${var.ec2_app_image_id}"
  ec2_app_instance_type                       = "${var.ec2_app_instance_type}"
  ec2_app_key_name                            = "${var.ec2_app_key_name}"
  # Subnets
  private_subnet_cidr_az1_internal_app        = "${module.network.private_subnet_cidr_az1_internal_app}"
  private_subnet_cidr_az2_internal_app        = "${module.network.private_subnet_cidr_az2_internal_app}"
  # Security
  sg_ec2_app                                  = "${module.security.sg_ec2_app}"
}
#########################################################################
#     Compute ECS - ECS Cluster, Services, Tasks and Load balancer      #
#########################################################################
module "compute_ecs" {
  source                                      = "../../resources/aws/compute_ecs/"
  # VPC data
  env_id                                      = "${var.env_id}"
  vpc_id                                      = "${module.network.vpc_id}"
  aws_region                                  = "${var.aws_region}"
  # IAM
  iam_role_ecs_service                        = "${module.iam.iam_role_ecs_service}"
  # ECS data
  ecs_cluster_name                            = "${var.ecs_cluster_name}"
  ecs_enable_container_insights               = "${var.ecs_enable_container_insights}"
  ecs_service_desired_count_generic           = "${var.ecs_service_desired_count_generic}"
  ecs_service_name_generic                    = "${var.ecs_service_name_generic}"
  ecs_task_definition_generic                 = "${var.ecs_task_definition_generic}"
  ecr_generic_name                            = "${module.repositories.ecr_generic_name}"
  # LB
  ecs_load_balancer_generic_name              = "${var.ecs_load_balancer_generic_name}"
  ecs_load_balancer_tg_generic_main_name      = "${var.ecs_load_balancer_tg_generic_main_name}"
  # Security
  sg_ecs_load_balancer                        = "${module.security.sg_ecs_load_balancer}"
  # Subnets
  private_subnet_cidr_az1_internal_data       = "${module.network.private_subnet_cidr_az1_internal_data}"
  private_subnet_cidr_az2_internal_data       = "${module.network.private_subnet_cidr_az2_internal_data}"
}
#########################################################################
#           Compute EKS - EKS Master, Node and Load balancer            #
#########################################################################
module "compute_ecs" {
  source                                      = "../../resources/aws/compute_eks/"
  # IAM
  generic_eks_master_role_arn                 = "${module.iam.generic_eks_master_role_arn}"
  aws_iam_instance_profile_node_name          = "${module.iam.aws_iam_instance_profile_node_name}"
  # Security groups
  sg_generic_eks_master                       = "${module.security.sg_generic_eks_master}"
  sg_generic_eks_node                         = "${module.security.sg_generic_eks_node}"
  # EKS data
  generic_eks_image_id                        = "${var.generic_eks_image_id}"
  generic_eks_cluster_name                    = "${var.generic_eks_cluster_name}"
  generic_eks_key_name                        = "${var.generic_eks_key_name}"
  generic_eks_node_instance_type              = "${var.generic_eks_node_instance_type}"
  generic_eks_node_name                       = "${var.generic_eks_node_name}"
  generic_eks_endpoint_private_access         = "${var.generic_eks_endpoint_private_access}"
  generic_eks_endpoint_public_access          = "${var.generic_eks_endpoint_public_access}"
  # Load balancer
  eks_load_balancer_generic_name              = "${var.eks_load_balancer_generic_name}"
  sg_eks_load_balancer                        = "${module.security.sg_eks_load_balancer}"
  # Subnets
  private_subnet_cidr_az1_internal_app        = "${module.network.private_subnet_cidr_az1_internal_app}"
  private_subnet_cidr_az2_internal_app        = "${module.network.private_subnet_cidr_az2_internal_app}"
  # RDS data
  #db_instance_endpoint                        = "${module.data.db_instance_endpoint}"
  #rds_username                                = "${var.rds_username}"
  #rds_password                                = "${var.rds_password}"
}
#########################################################################
#                         Storage - S3 buckets                          #
#########################################################################
module "storage" {
  source                                      = "../../resources/aws/storage/"
  statefile_bucket_name                       = "${var.statefile_bucket_name}"
}
#########################################################################
#                         VPN - VPN connections                         #
#########################################################################
module "vpn" {
  source                                      = "../../resources/aws/vpn/"
  # VPC data
  vpc_id                                      = "${module.network.vpc_id}"
}
#########################################################################
#                     SNS - SNS topics and subscriptions                #
#########################################################################
module "sns" {
  source                                      = "../../resources/aws/sns/"
  # SNS data
  generic_sns_topic_name                      = "${var.generic_sns_topic_name}"
}
