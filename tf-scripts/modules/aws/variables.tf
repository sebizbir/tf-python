#########################################################################
#                                DEFAULT                                #
#########################################################################
variable "aws_access_key" {}
variable "aws_secret_key" {}
variable "aws_key_path" {}
variable "aws_key_name" {}

variable "aws_region" {
  description 	= "EC2 Region for the VPC"
  default 		= "eu-west-2"
}

variable "env_id" {
  description   = "Environment name"
  default       = "Generic Environment"
}
#########################################################################
#                                NETWORK                                #
#########################################################################
#########################################################
#                         VPC                           #
#########################################################
variable "vpc_cidr" {
  description 	= "CIDR for the VPC"
  default 		= "10.63.64.0/22"
}
variable "vpc_dns" {
  description   = "Enable dns hostnames"
  default       = true
}
#########################################################
#                         Subnets                       #
#########################################################
################################################
#                   Public                     #
################################################
variable "public_subnet_cidr_az1" {
  description 	= "CIDR for the Public Subnet - External Subnet - AZ1"
  default 		= "10.63.64.0/26"
}

variable "public_subnet_cidr_az2" {
  description 	= "CIDR for the Public Subnet - External Subnet - AZ2"
  default 		= "10.63.64.64/26"
}
################################################
#                  Private                     #
################################################
variable "private_subnet_cidr_az1_internal_app" {
  description 	= "CIDR for the Private App Subnet - Internal Subnet - AZ1"
  default 		= "10.63.2.0/26"
}

variable "private_subnet_cidr_az2_internal_app" {
  description 	= "CIDR for the Private App Subnet - Internal Subnet - AZ2"
  default 		= "10.63.2.64/26"
}
variable "private_subnet_cidr_az1_internal_data" {
  description 	= "CIDR for the Private Data Subnet - Internal Subnet - AZ1"
  default 		= "10.63.3.0/26"
}

variable "private_subnet_cidr_az2_internal_data" {
  description 	= "CIDR for the Private Data Subnet - Internal Subnet - AZ2"
  default 		= "10.63.3.64/26"
}
#########################################################################
#                                COMPUTE                                #
#########################################################################
#########################################################
#                         EC2                           #
#########################################################
variable "ec2_app_image_id" {
  description = "AMI of the instance"
  default     = ""
}
variable "ec2_app_instance_type" {
  description = "Instance type (t2.micro)"
  default     = "t2.micro"
}
variable "ec2_app_key_name" {
  description = "Key pair for instances"
  default     = "ec2-generic-keys"
}
####################################################################
#                               ECS                                #
#  https://www.terraform.io/docs/providers/aws/r/ecs_service.html  #
####################################################################
variable "ecs_cluster_name" {
  description = "Name for ECS cluster"
  default      = "GenericECS"
}
variable "ecs_enable_container_insights" {
  description = "Enabled/Disabled, container Insights for ECS cluster"
  default 	  = "Enabled"
}
variable "ecs_service_desired_count_generic" {
  description = "Desired count of running tasks for ECS service"
  default     = 1
}
variable "ecs_service_name_generic" {
  description = "Name for ECS service"
  default     = "generic_service"
}
variable "ecs_task_definition_generic" {
  description = "Name for ECS Task definition"
  default     = "generic_task_definition"
}
variable "ecs_load_balancer_generic_name" {
  description = "Name for ECS Application Load balancer"
  default     = "generic_lb_name"
}
variable "ecs_load_balancer_tg_generic_main_name" {
  description = "Name for Target group for Application Load balancer"
  default     = "generic_lb_tg"
}
####################################################################
#                               EKS                                #
#  https://www.terraform.io/docs/providers/aws/r/eks_cluster.html  #
####################################################################
variable "generic_eks_cluster_name" {
  description = "Name for EKS cluster"
  default     = "GenericEKS"
}
variable "generic_eks_image_id" {
  description 	= "Image for EKS nodes"
  default 		= "ami-0f454b09349248e29"
}
variable "generic_eks_node_instance_type" {
  description = "EKS node instance type"
  default     = "m4.large"
}
variable "generic_eks_node_name" {
  description = "Name for EKS nodes"
  default     = "generic-eks"
}
variable "generic_eks_key_name" {
  description = "Key pair for nodes"
  default     = "eks-generic-nodes"
}
variable "generic_eks_endpoint_private_access" {
  description = "Enable/Disable private EKS endpoint"
  default     = true
}
variable "generic_eks_endpoint_public_access" {
  description = "Enable/Disable public EKS endpoint"
  default     = true
}
variable "eks_load_balancer_generic_name" {
  description = "Name for EKS Load balancer"
  default = "eks-load-balancer"
}
#########################################################################
#                             SECURITY                                  #
#########################################################################
variable "sg_ecs_load_balancer_name" {
  description = "Security group name for ECS Load balancer"
  default     = "sg_ecs_load_balancer"
}
variable "sg_eks_load_balancer_name" {
  description = "Security group name for EKS Load balancer"
  default     = "sg_eks_load_balancer"
}
variable "sg_rds_mssql_database_name" {
  description = "Security group name for RDS MSSQL database"
  default     = "sg_rds_mssql_database"
}
variable "sg_rds_mysql_database_name" {
  description = "Security group name for RDS MySQL database"
  default     = "sg_rds_mysql_database"
}
#########################################################################
#                             REPOSITORIES                              #
#########################################################################
variable "ecr_generic_name" {
  description = "Name for ECR repository"
  default     = "ecr_generic_repo"
}
variable "ecr_generic_mutability" {
  description = "ECR repository mutability"
  default     = "MUTABLE"
}
#########################################################################
#                                  DATA                                 #
#     https://www.terraform.io/docs/providers/aws/r/db_instance.html    #
#########################################################################
#####################################
#               MSSQL               #
#####################################
variable "rds_mssql_identifier" {
  description = "Database identifier"
  default     = "terraform-generic"
}
variable "rds_mssql_allocated_storage" {
  description = "Allocated storage in GB"
  default     = 30
}
variable "rds_mssql_backup_retention_period" {
  description = "Retention period"
  default     = 7
}
variable "rds_mssql_backup_window" {
  description = "Backup window (should be >=30 mins)"
  default     = "01:37-02:07"
}
variable "rds_mssql_maintenance_window" {
  description = "Maintenance window (should be >=30 mins)"
  default     = "Mon:23:29-Mon:23:59"
}
variable "rds_mssql_multi_az" {
  description = "Enable/disable Multi AZ failover option"
  default     = true
}
variable "rds_mssql_storage_encrypted" {
  description = "Encrypt/Not encrypt underlying storage"
  default     = true
}
variable "rds_mssql_publicly_accessible" {
  description = "Enable public/private access"
  default    = false
}
variable "rds_mssql_storage_type" {
  description = "Underlying storage type"
  default     = "gp2"
}
variable "rds_mssql_engine" {
  description = "MSSQL engine type"
  default     = "sqlserver-se"
}
variable "rds_mssql_engine_version" {
  description = "MSSQL engine version"
  default     = "13.00.5292.0.v1"
}
variable "rds_mssql_license_model" {
  description = "MSSQL license model"
  default     = "license-included"
}
variable "rds_mssql_instance_class" {
  description = "RDS instance class for the underlying machine"
  default     = "db.m5.xlarge"
}
variable "rds_mssql_username" {
  description = "Initial user"
  default     = "dbGeneric"
}
variable "rds_mssql_password" {
  description = "Initial password"
  default     = "dbGeneric2019"
}
#####################################
#               MySQL               #
#####################################
variable "rds_mysql_identifier" {
  description = "Database identifier"
  default     = "terraform-generic"
}
variable "rds_mysql_allocated_storage" {
  description = "Allocated storage in GB"
  default     = 20
}
variable "rds_mysql_backup_retention_period" {
  description = "Retention period"
  default     = 7
}
variable "rds_mysql_backup_window" {
  description = "Backup window (should be >=30 mins)"
  default     = "01:37-02:07"
}
variable "rds_mysql_maintenance_window" {
  description = "Maintenance window (should be >=30 mins)"
  default     = "Mon:23:29-Mon:23:59"
}
variable "rds_mysql_multi_az" {
  description = "Enable/disable Multi AZ failover option"
  default     = true
}
variable "rds_mysql_storage_encrypted" {
  description = "Encrypt/Not encrypt underlying storage"
  default     = true
}
variable "rds_mysql_publicly_accessible" {
  description = "Enable public/private access"
  default     = false
}
variable "rds_mysql_storage_type" {
  description = "Underlying storage type"
  default     = "gp2"
}
variable "rds_mysql_engine" {
  description = "MySQL engine type"
  default     = "mysql"
}
variable "rds_mysql_engine_version" {
  description = "MySQL engine version"
  default     = "5.5.61"
}
variable "rds_mysql_instance_class" {
  description = "RDS instance class for the underlying machine"
  default     = "db.m5.xlarge"
}
variable "rds_mysql_db_name" {
  description = "MySQL database name"
  default     = "dbCAGateway"
}
variable "rds_mysql_username" {
  description = "Initial user"
  default     = "dbCAGateway"
}
variable "rds_mysql_password" {
  description = "Initial password"
  default     = "dbCAGateway2019"
}
variable "rds_mysql_family_name" {
  description = "MySQL database family name"
  default     = "mysql5.5"
}
####################################################################################
#                             ACTIVE DIRECTORY                                     #
#  https://www.terraform.io/docs/providers/aws/r/directory_service_directory.html  #
####################################################################################
variable "generic_ad_name" {
  description = "Active Directory name"
  default     = "generic.internal"
}
variable "generic_ad_short_name" {
  description = "Active Directory short name (would appear as name/user)"
  default     = "generic"
}
variable "generic_ad_password" {
  description = "Initial AD password"
  default     = "generic2019"
}
variable "generic_ad_edition" {
  description = "Active Directory edition"
  default     = "Standard"
}
variable "generic_ad_type" {
  description = "Active Directory type"
  default     = "MicrosoftAD"
}
#########################################################################
#                               STORAGE                                 #
#########################################################################
variable "statefile_bucket_name" {
  description = "S3 bucket name"
  default     = "statefile_bucket"
}
#########################################################################
#                           SNS notifications                           #
#########################################################################
variable "generic_sns_topic_name" {
  description = "SNS topic name"
  default     = "generic_sns_topic"
}