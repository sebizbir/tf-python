module "network" {
  source                                  = "../../resources/gcp/network/"
  # VPC data
  network_name                            = "${var.network_name}"
  vpc_subnetworks                         = "${var.vpc_subnetworks}"
}

module "security" {
  source                                  = "../../resources/gcp/security/"
  # VPC data
  network_name                            = "${var.network_name}"
  # FW - http/https
  generic_http_https_allow_protocol       = "${var.generic_http_https_allow_protocol}"
  generic_http_https_allow_protocol_ports = "${var.generic_http_https_allow_protocol_ports}"
  generic_http_https_firewall_name        = "${var.generic_http_https_firewall_name}"
  generic_http_https_source_ranges        = "${var.generic_http_https_source_ranges}"
  generic_http_https_source_tags          = "${var.generic_http_https_source_tags}"
  generic_http_https_target_tags          = "${var.generic_http_https_target_tags}"
  # FW - ssh
  generic_ssh_allow_protocol              = "${var.generic_ssh_allow_protocol}"
  generic_ssh_allow_protocol_ports        = "${var.generic_ssh_allow_protocol_ports}"
  generic_ssh_firewall_name               = "${var.generic_ssh_firewall_name}"
  generic_ssh_source_ranges               = "${var.generic_ssh_source_ranges}"
  generic_ssh_source_tags                 = "${var.generic_ssh_source_tags}"
  generic_ssh_target_tags                 = "${var.generic_ssh_target_tags}"
}

module "compute" {
  source                                  = "../../resources/gcp/compute/"
  # Single Instances data
  instance_generic_name                   = "${var.instance_generic_name}"
  instance_generic_type                   = "${var.instance_generic_type}"
  instance_generic_zone                   = "${var.instance_generic_zone}"
  instance_generic_tags                   = "${var.instance_generic_tags}"
  instance_generic_network                = "${module.network.network_generic_name}"
  # Instances
  generic_project_id                      = "${var.generic_project_id}"
  instance_group_generic_name             = "${var.instance_group_generic_name}"
  # CloudRun data
  cloud_run_service_name                  = "${var.cloud_run_service_name}"
  cloud_run_service_location              = "${var.cloud_run_service_location}"
  cloud_run_service_namespace             = "${var.cloud_run_service_namespace}"
  cloud_run_service_image                 = "${var.cloud_run_service_image}"
}

module "storage" {
  source                                  = "../../resources/gcp/storage/"
  # Storage buckets
  storage_bucket_generic_name             = "${var.storage_bucket_generic_name}"
  storage_bucket_generic_location         = "${var.storage_bucket_generic_location}"
  storage_bucket_generic_class            = "${var.storage_bucket_generic_class}"
  storage_bucket_generic_versioning       = "${var.storage_bucket_generic_versioning}"
}

module "data" {
  source                                  = "../../resources/gcp/data/"
  # CloudSQL - MySQL
  # DB Instance
  mysql_generic_database_instance_name    = "${var.storage_bucket_generic_name}"
  mysql_generic_database_version          = "${var.storage_bucket_generic_name}"
  mysql_generic_database_region           = "${var.storage_bucket_generic_name}"
  mysql_generic_database_tier             = "${var.storage_bucket_generic_name}"
  mysql_generic_database_ipv4_enabled     = "${var.storage_bucket_generic_name}"
  mysql_generic_database_private_network  = "${module.network.network_generic_name}"
  # Database
  mysql_generic_database_name             = "${var.storage_bucket_generic_name}"
  # Users
  mysql_generic_sql_user_name             = "${var.storage_bucket_generic_name}"
  mysql_generic_sql_user_password         = "${var.storage_bucket_generic_name}"
}