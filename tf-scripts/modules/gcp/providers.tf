provider "google" {
  credentials = file("../../access/gcp/igneous-effort-257218-e75112216dbf.json")
#  credentials = $GCP_ACCESS
  project     = "igneous-effort-257218"
  region      = "europe-west1"
}

provider "google-beta" {
 # credentials = file("../../access/gcp/igneous-effort-257218-509effb52e24.json")
 # credentials = $GCP_ACCESS
  project     = "igneous-effort-257218"
  region      = "europe-west1"
}