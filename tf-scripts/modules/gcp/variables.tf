#########################################################################
#                                Network                                #
#########################################################################
#########################################################
#                          VPC                          #
#########################################################
variable "generic_project_id" {}
variable "network_name" {
  default = "test2"
}
variable "vpc_subnetworks" {
  default = true
}
#########################################################
#                        Firewall                       #
#########################################################
variable "generic_ssh_firewall_name" {}
variable "generic_http_https_firewall_name" {}
variable "generic_ssh_allow_protocol" {}
variable "generic_ssh_allow_protocol_ports" {}
variable "generic_http_https_allow_protocol" {}
variable "generic_http_https_allow_protocol_ports" {}
variable "generic_http_https_target_tags" {}
variable "generic_http_https_source_ranges" {}
variable "generic_http_https_source_tags" {}
variable "generic_ssh_target_tags" {}
variable "generic_ssh_source_ranges" {}
variable "generic_ssh_source_tags" {}
#########################################################################
#                                Compute                                #
#########################################################################
#########################################################
#                       Instances                       #
#########################################################
# Single instances
variable "instance_generic_name" {}
variable "instance_generic_type" {}
variable "instance_generic_zone" {}
variable "instance_generic_tags" {}
# Instance groups
variable "instance_group_generic_name" {}
#########################################################
#                       CloudRun                       #
#########################################################
variable "cloud_run_service_name" {
  default = "tf-python-cloudrun"
  description = "Name of Cloud Run service"
}
variable "cloud_run_service_location" {
  default = "europe-west1"
  description = "Region for Cloud Run service"
}
variable "cloud_run_service_namespace" {
  default = "igneous-effort-257218"
  description = "Namespace must be equal to either the project ID or project number"
}
variable "cloud_run_service_image" {
  default = "gcr.io/igneous-effort-257218/helloworld"
}
#########################################################################
#                                Storage                                #
#########################################################################
#########################################################
#                        Buckets                        #
#########################################################
variable "storage_bucket_generic_name" {}
variable "storage_bucket_generic_location" {}
variable "storage_bucket_generic_class" {}
variable "storage_bucket_generic_versioning" {}
#########################################################################
#                                  Data                                 #
#########################################################################
#########################################################
#                          MySQL                        #
#########################################################
# CloudSQL - MySQL
# DB Instance
variable "mysql_generic_database_instance_name" {}
variable "mysql_generic_database_version" {}
variable "mysql_generic_database_region" {}
variable "mysql_generic_database_tier" {}
variable "mysql_generic_database_ipv4_enabled" {}
variable "mysql_generic_database_private_network" {}
# Database
variable "mysql_generic_database_name" {}
# Users
variable "mysql_generic_sql_user_name" {}
variable "mysql_generic_sql_user_password" {}