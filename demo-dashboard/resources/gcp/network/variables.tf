variable "gcp_demo_dashboard_api_compute" {}
variable "statefile_bucket_name" {}
variable "gcp_domain_name" {}
variable "network_name" {}
variable "vpc_subnetworks" {}

variable "enable_ssl" {}
variable "demo_project_id" {}
variable "demo_project_region" {}
