// self signed SSL
/*
resource "tls_self_signed_cert" "demo_dashboard_lb_self_cert" {
  # Only create if SSL is enabled
  count = var.enable_ssl ? 1 : 0

  key_algorithm   = "RSA"
  private_key_pem = join("", tls_private_key.demo_dashboard_lb_private_key.*.private_key_pem)

  subject {
    common_name  = var.gcp_domain_name
    organization = "Form"
  }
  dns_names = [
    "${var.gcp_domain_name}","www.${var.gcp_domain_name}","bucket.${var.gcp_domain_name}","www.bucket.${var.gcp_domain_name}",".${var.gcp_domain_name}"
  ]

  validity_period_hours = 12

  allowed_uses = [
    "key_encipherment",
    "digital_signature",
    "server_auth",
  ]
}
// tls key
resource "tls_private_key" "demo_dashboard_lb_private_key" {
  # Only create if SSL is enabled
  count = var.enable_ssl ? 1 : 0

  algorithm   = "RSA"
  ecdsa_curve = "P256"
}

// ssl on google cloud
resource "google_compute_ssl_certificate" "demo_dashboard_lb_certificate" {
  # Only create if SSL is enabled
  count = var.enable_ssl ? 1 : 0

  project = var.demo_project_id

  name_prefix = "demo-dashboard-ssl-"
  description = "SSL Certificate for https Load Balancer"
  private_key = join("", tls_private_key.demo_dashboard_lb_private_key.*.private_key_pem)
  certificate = join("", tls_self_signed_cert.demo_dashboard_lb_self_cert.*.cert_pem)

  lifecycle {
    create_before_destroy = true
  }
}
*/

// google managed cert
resource "google_compute_managed_ssl_certificate" "demo_dashboard_lb_certificate" {
  provider = google-beta
  count      = var.enable_ssl ? 1 : 0
  name = "demo-dashboard-ssl-lb"

  managed {
    domains = [var.gcp_domain_name]
  }
}



/*
// ip address for load balancer - for global
resource "google_compute_address" "lb_http_address" {
  project = var.demo_project_id
  name = "glb-static-address"
  ip_version = "IPV4"
}
*/
// ip address for load balancer - for regional
resource "google_compute_address" "lb_http_address" {
  name = "glb-static-address"
  address_type = "EXTERNAL"
  region = var.demo_project_region
  project = var.demo_project_id
  network_tier = "STANDARD"
}


// load balancer - frontend to google_compute_global_forwarding_rule
resource "google_compute_forwarding_rule" "demo_dashboard_http_forwarding_rule" {
  project    = var.demo_project_id
  name       = "demo-dashboard-http"
  target     = google_compute_target_http_proxy.demo_dashboard_http_proxy.self_link
  port_range = "80"
  network_tier = "STANDARD"
  ip_address = google_compute_address.lb_http_address.address
}
// https
resource "google_compute_forwarding_rule" "demo_dashboard_https_forwarding_rule" {
  project    = var.demo_project_id
  count      = var.enable_ssl ? 1 : 0
  name       = "demo-dashboard-https"
  target     = google_compute_target_https_proxy.demo_dashboard_https_proxy[0].self_link
  port_range = "443"
  network_tier = "STANDARD"
  ip_address = google_compute_address.lb_http_address.address
}

// http
resource "google_compute_target_http_proxy" "demo_dashboard_http_proxy" {
  project = var.demo_project_id
  name    = "demo-dashboard-http-proxy"
  url_map = google_compute_url_map.demo_dashboard_urlmap.name
}

// https
resource "google_compute_target_https_proxy" "demo_dashboard_https_proxy" {
  count      = var.enable_ssl ? 1 : 0
  project = var.demo_project_id
  name    = "demo-dashboard-https-proxy"
  url_map = google_compute_url_map.demo_dashboard_urlmap.name
  ssl_certificates = [google_compute_managed_ssl_certificate.demo_dashboard_lb_certificate[0].name]
}

// load balancer - url map
resource "google_compute_url_map" "demo_dashboard_urlmap" {
  name        = "demo-dashboard-urlmap"

  default_service = google_compute_backend_bucket.statefile_bucket_backend.self_link

  host_rule {
    hosts        = [
      var.gcp_domain_name]
    path_matcher = "allpaths"
  }

  path_matcher {
    name            = "allpaths"
    default_service = google_compute_backend_bucket.statefile_bucket_backend.self_link

    path_rule {
      paths   = ["/bucket/*"]
      service = google_compute_backend_bucket.statefile_bucket_backend.self_link
    }
  }
}



// static website backend
resource "google_compute_backend_bucket" "statefile_bucket_backend" {
  name        = "statefile-bucket-backend"
  bucket_name = var.statefile_bucket_name
}
