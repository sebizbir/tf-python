output "cloud_run_demo_dashboard_main_url" {
  value = google_cloud_run_service.cloud_run_demo_dashboard_main.status[0].url
}
output "cloud_run_demo_dashboard_domain_mapping" {
  value = google_cloud_run_domain_mapping.cloud_run_demo_dashboard_domain_mapping.status[0].resource_records
}
/*
output "cloud_run_demo_dashboard_domain_mapping_type" {
  value = google_cloud_run_domain_mapping.cloud_run_demo_dashboard_domain_mapping.status.conditions[type]
}
*/