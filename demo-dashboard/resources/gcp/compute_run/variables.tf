# Project
variable "demo_project_id" {}
variable "gcp_domain_name" {}
# Cloud Run
variable "cloud_run_demo_dashboard_service_name" {}
variable "cloud_run_demo_dashboard_service_location" {}
variable "cloud_run_demo_dashboard_service_namespace" {}
variable "cloud_run_demo_dashboard_service_image" {}
