#################################################################################
#                                 Cloud Run instances                           #
#################################################################################
########################################
#                Service               #
########################################
resource "google_cloud_run_service" "cloud_run_demo_dashboard_main" {
  provider = "google-beta"
  name     = var.cloud_run_demo_dashboard_service_name
  location = var.cloud_run_demo_dashboard_service_location

  metadata {
    namespace = var.cloud_run_demo_dashboard_service_namespace
  }

  spec {
    containers {
      image = "${var.cloud_run_demo_dashboard_service_image}"
    }
  }
}

resource "google_cloud_run_domain_mapping" "cloud_run_demo_dashboard_domain_mapping" {
  location = var.cloud_run_demo_dashboard_service_location
  name     = "app.${var.gcp_domain_name}"

  metadata {
    namespace = var.cloud_run_demo_dashboard_service_namespace
  }

  spec {
    route_name = google_cloud_run_service.cloud_run_demo_dashboard_main.name
  }

  depends_on = [google_cloud_run_service.cloud_run_demo_dashboard_main]
}

########################################
#             Service IAM              #
########################################
data "google_iam_policy" "cloud_run_demo_dashboard_noauth" {
  binding {
    role = "roles/run.invoker"
    members = [
      "allUsers",
    ]
  }
}

resource "google_cloud_run_service_iam_policy" "cloud_run_demo_dashboard_noauth" {
  location    = google_cloud_run_service.cloud_run_demo_dashboard_main.location
  project     = var.demo_project_id
  service     = google_cloud_run_service.cloud_run_demo_dashboard_main.name

  policy_data = data.google_iam_policy.cloud_run_demo_dashboard_noauth.policy_data
}
