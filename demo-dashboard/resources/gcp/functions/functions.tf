// functions

resource "google_cloudfunctions_function" "function_static_website" {
  name        = "function_static_website"
  description = "Python function for static website"
  runtime     = "python37"
  service_account_email = var.terraform_service_account_email

  available_memory_mb   = 128
  #source_archive_bucket = var.functions_bucket_name
  #source_archive_object = var.functions_bucket_object_1_name
  source_archive_bucket = "gcp-demo-dashboard-functions"
  source_archive_object = "main.zip"
  trigger_http          = true
  labels      = {
    environment = var.env_id
  }

  depends_on = [var.gcp_demo_dashboard_api_cloudfunctions, var.functions_bucket_name, var.functions_bucket_object_1_name]
}
/*
resource "google_cloudfunctions_function_iam_member" "function_static_website_invoker" {
  project        = var.demo_project_id
  region         = var.demo_project_region
  cloud_function = google_cloudfunctions_function.function_static_website.name

  role   = "roles/cloudfunctions.invoker"
  member = "allUsers"
}
*/