variable "terraform_service_account_email" {}
variable "gcp_demo_dashboard_api_cloudfunctions" {}
variable "env_id" {}
variable "functions_bucket_name" {}
variable "functions_bucket_object_1_name" {}
variable "demo_project_id" {}
variable "demo_project_region" {}