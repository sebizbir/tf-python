# Storage buckets
variable "statefile_bucket_name" {}
variable "statefile_bucket_location" {}
variable "statefile_bucket_class" {}

variable "functions_bucket_name" {}
variable "functions_bucket_location" {}
variable "functions_bucket_class" {}