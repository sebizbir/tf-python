#################################################################################
#                                      Storage                                  #
#################################################################################
########################################
#               Buckets                #
########################################
// bucket for static website
resource "google_storage_bucket" "statefile_bucket" {
  name     = var.statefile_bucket_name
  location = var.statefile_bucket_location
  storage_class = var.statefile_bucket_class

  force_destroy = true
  website {
    main_page_suffix = "index.html"
    not_found_page   = "error.html"
  }
}
// access
resource "google_storage_bucket_access_control" "statefile_bucket_public_rule" {
  bucket = google_storage_bucket.statefile_bucket.name
  role   = "READER"
  entity = "allUsers"

  depends_on = [google_storage_bucket.statefile_bucket]
}
// files
resource "google_storage_bucket_object" "statefile_bucket_object_1" {
  name   = "index.html"
  source = "../../resources/gcp/storage/static_website/index.html"
  bucket = google_storage_bucket.statefile_bucket.name

  depends_on = [google_storage_bucket.statefile_bucket]
}
resource "google_storage_bucket_object" "statefile_bucket_object_2" {
  name   = "error.html"
  source = "../../resources/gcp/storage/static_website/error.html"
  bucket = google_storage_bucket.statefile_bucket.name

  depends_on = [google_storage_bucket.statefile_bucket]
}
resource "google_storage_bucket_object" "statefile_bucket_object_3" {
  name   = "styles.css"
  source = "../../resources/gcp/storage/static_website/styles.css"
  bucket = google_storage_bucket.statefile_bucket.name

  depends_on = [google_storage_bucket.statefile_bucket]
}
// access
resource "google_storage_object_access_control" "statefile_bucket_object_1_public_rule" {
  object = google_storage_bucket_object.statefile_bucket_object_1.output_name
  bucket = google_storage_bucket.statefile_bucket.name
  role   = "READER"
  entity = "allUsers"

  depends_on = [google_storage_bucket.statefile_bucket, google_storage_bucket_object.statefile_bucket_object_1]
}
resource "google_storage_object_access_control" "statefile_bucket_object_2_public_rule" {
  object = google_storage_bucket_object.statefile_bucket_object_2.output_name
  bucket = google_storage_bucket.statefile_bucket.name
  role   = "READER"
  entity = "allUsers"

  depends_on = [google_storage_bucket.statefile_bucket, google_storage_bucket_object.statefile_bucket_object_2]
}
resource "google_storage_object_access_control" "statefile_bucket_object_3_public_rule" {
  object = google_storage_bucket_object.statefile_bucket_object_3.output_name
  bucket = google_storage_bucket.statefile_bucket.name
  role   = "READER"
  entity = "allUsers"

  depends_on = [google_storage_bucket.statefile_bucket, google_storage_bucket_object.statefile_bucket_object_3]
}


// bucket for Cloud Functions code
resource "google_storage_bucket" "functions_bucket" {
  name     = var.functions_bucket_name
  location = var.functions_bucket_location
  storage_class = var.functions_bucket_class
  force_destroy = true
}

// archive function
data "archive_file" "static_zip" {
  type        = "zip"
  source_file = "../../resources/gcp/storage/static_website/main.py"
  output_path = "../../resources/gcp/storage/static_website/main.zip"
}

resource "google_storage_bucket_object" "functions_bucket_object_1" {
  name   = "main.zip"
  source = "../../resources/gcp/storage/static_website/main.zip"
  bucket = google_storage_bucket.functions_bucket.name

  depends_on = [google_storage_bucket.functions_bucket]
}
