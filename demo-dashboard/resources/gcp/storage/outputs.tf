output "functions_bucket_name" {
  value = google_storage_bucket.functions_bucket.name
}
output "functions_bucket_object_1_name" {
  value = google_storage_bucket_object.functions_bucket_object_1.name
}
output "statefile_bucket_name" {
  value = google_storage_bucket.statefile_bucket.name
}