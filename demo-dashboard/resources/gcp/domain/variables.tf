variable "gcp_demo_dashboard_api_dns" {}
variable "env_id" {}
variable "gcp_domain_name" {}
variable "demo_dashboard_lb_http_address" {}
variable "cloud_run_demo_dashboard_main_url" {}
variable "cloud_run_demo_dashboard_domain_mapping" {}