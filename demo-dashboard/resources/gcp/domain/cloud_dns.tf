// zone
resource "google_dns_managed_zone" "gcp_zone_demo_dashboard" {
  name        = "gcp-dashboard-ml"
  dns_name    = "${var.gcp_domain_name}."
  labels      = {
    environment = var.env_id
  }

  depends_on = [var.gcp_demo_dashboard_api_dns]
}

// point CNAME to GCS
resource "google_dns_record_set" "demo_bucket_cname" {
  name         = "bucket.${google_dns_managed_zone.gcp_zone_demo_dashboard.dns_name}"
  managed_zone = google_dns_managed_zone.gcp_zone_demo_dashboard.name
  type         = "CNAME"
  ttl          = 300
  rrdatas      = ["c.storage.googleapis.com."]

  depends_on = [google_dns_managed_zone.gcp_zone_demo_dashboard]
}

// point CNAME to LB
resource "google_dns_record_set" "demo_http_load_balancer_a_record" {
  name         = google_dns_managed_zone.gcp_zone_demo_dashboard.dns_name
  managed_zone = google_dns_managed_zone.gcp_zone_demo_dashboard.name
  type         = "A"
  ttl          = 300
  rrdatas      = [var.demo_dashboard_lb_http_address]

  depends_on = [google_dns_managed_zone.gcp_zone_demo_dashboard]
}


// point CNAME to CloudRun

resource "google_dns_record_set" "demo_cloud_run_service" {
  name         = "app.${google_dns_managed_zone.gcp_zone_demo_dashboard.dns_name}"
  managed_zone = google_dns_managed_zone.gcp_zone_demo_dashboard.name
  type         = "CNAME"
  ttl          = 300
  rrdatas      = ["ghs.googlehosted.com."]

  depends_on = [google_dns_managed_zone.gcp_zone_demo_dashboard]
}
/*
resource "google_dns_record_set" "demo_cloud_run_service" {
  name         = "app.${google_dns_managed_zone.gcp_zone_demo_dashboard.dns_name}"
  managed_zone = google_dns_managed_zone.gcp_zone_demo_dashboard.name
  type         = "CNAME"
  ttl          = 300
  rrdatas      = [var.cloud_run_demo_dashboard_domain_mapping]

  depends_on = [google_dns_managed_zone.gcp_zone_demo_dashboard]
}
*/