# Project
variable "demo_project_id" {}
# Single Instances

variable "instance_generic_name" {}
variable "instance_generic_type" {}
variable "instance_generic_zone" {}
variable "instance_generic_tags" {}
variable "instance_generic_network" {}

# Instance groups
variable "instance_group_generic_name" {}
