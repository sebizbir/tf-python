#################################################################################
#                                 Compute instances                             #
#################################################################################
########################################
#         Single Instance              #
########################################
/*
resource "google_compute_instance" "instance_generic" {
  name         = var.instance_generic_name
  machine_type = var.instance_generic_type
  zone         = var.instance_generic_zone

  tags = [var.instance_generic_tags]

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-9"
    }
  }

  // Local SSD disk
  scratch_disk {
    interface = "SCSI"
  }

  network_interface {
    network = var.instance_generic_network
    #network_ip = ""
    access_config {
      // Ephemeral IP
    }
  }

  service_account {
    scopes = ["cloud-platform"]  # cloud-platform = all api's (https://cloud.google.com/sdk/gcloud/reference/alpha/compute/instances/set-scopes#--scopes)
  }
}
*/
########################################
#         Instance template            #
########################################
/*
resource "google_compute_instance_template" "generic_template" {
  name        = var.instance_group_generic_name
  description = "This template is used to create app server instances."

  tags = [var.instance_generic_tags]

  labels = {
    environment = "dev"
  }

  instance_description = "App server instance"
  machine_type         = var.instance_generic_type
  can_ip_forward       = false

  scheduling {
    automatic_restart   = true
    on_host_maintenance = "MIGRATE"
  }

  // Create a new boot disk from an image
  disk {
    source_image = "debian-cloud/debian-9"
    auto_delete  = true
    boot         = true
  }
  // Additional disk
  disk {
    // Instance Templates reference disks by name, not self link
    source      = google_compute_disk.generic_disk.name
    auto_delete = true
    boot        = false
  }

  network_interface {
    network = var.instance_generic_network
    access_config {
      // Ephemeral IP
    }
  }

  service_account {
    scopes = ["cloud-platform"]  # cloud-platform = all api's (https://cloud.google.com/sdk/gcloud/reference/alpha/compute/instances/set-scopes#--scopes)
  }
}
*/
/*
data "google_compute_image" "generic_image" {
  family  = "debian-9"
  project = var.generic_project_id
}

resource "google_compute_disk" "generic_disk" {
  name  = "non-boot-disk"
  image = data.google_compute_image.generic_image.self_link
  size  = 10
  type  = "pd-ssd"
  zone  = var.instance_generic_zone
}
*/
########################################
#         Instance group               #
########################################
/*
resource "google_compute_instance_group_manager" "generic_instance_group_manager" {
  name               = var.instance_group_generic_name
  instance_template  = google_compute_instance_template.generic_template.self_link
  base_instance_name = var.instance_group_generic_name
  zone               = var.instance_generic_zone
  target_size        = "1"
}
*/