# CloudSQL - demo_dashboard_mysql
variable "api_cloud_sql" {}
variable "api_service_networking" {}
# DB Instance
variable "demo_dashboard_mysql_database_instance_name" {}
variable "demo_dashboard_mysql_database_version" {}
variable "demo_dashboard_mysql_database_region" {}
variable "demo_dashboard_mysql_database_tier" {}
variable "demo_dashboard_mysql_database_ipv4_enabled" {}
variable "demo_dashboard_mysql_database_private_network" {}
# Database
variable "demo_dashboard_mysql_database_name" {}
# Users
variable "demo_dashboard_mysql_sql_user_name" {}
variable "demo_dashboard_mysql_sql_user_password" {}

