## Cloud Functions service account
/*
resource "google_service_account" "service_account_cf_serviceAgent" {
  account_id    = "service_account_id"
  display_name  = "Service Account"
  project       = var.demo_project_number

  depends_on = [var.gcp_demo_dashboard_api_iam]
}
*/
#service-"${var.project_number_demo_dashboard}"@gcf-admin-robot.iam.gserviceaccount.com

data "google_service_account" "terraform_service_account" {
  account_id  = "terraform"
  project     = var.demo_project_id
}
