output "terraform_service_account_email" {
  value = data.google_service_account.terraform_service_account.email
}
output "api_cloudresourcemanager" {
  value = google_project_service.demo_project_manager.service
}
output "api_dns" {
  value = google_project_service.demo_project_dns.service
}
output "api_iam" {
  value = google_project_service.demo_project_iam.service
}
output "api_cloudfunctions" {
  value = google_project_service.demo_project_functions.service
}
output "api_compute" {
  value = google_project_service.demo_project_compute.service
}
output "api_registry" {
  value = google_project_service.demo_project_registry.service
}
output "api_cloud_run" {
  value = google_project_service.demo_project_cloud_run.service
}
output "api_cloud_build" {
  value = google_project_service.demo_project_cloud_build.service
}
output "api_service_networking" {
  value = google_project_service.demo_project_service_networking.service
}
output "api_cloud_sql" {
  value = google_project_service.demo_project_cloud_sql.service
}