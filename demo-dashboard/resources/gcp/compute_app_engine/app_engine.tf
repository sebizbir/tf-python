/*
App Engine applications cannot be deleted once they're created;
you have to delete the entire project to delete the application.
Terraform will report the application has been successfully deleted;
this is a limitation of Terraform, and will go away in the future.
Terraform is not able to delete App Engine applications.
*/
//  https://github.com/yukuku/telebot/blob/master/app.yaml
resource "google_app_engine_application" "app_engine_app_telegram_bot" {
  project     = var.demo_project_id
  location_id = var.demo_project_region
}

resource "google_app_engine_standard_app_version" "app_engine_version_telegram_bot" {
  version_id = "v1"
  service    = "telegram-bot"
  runtime    = "python27"

  entrypoint {
    shell = "node ./app.js"
  }

  deployment {
    zip {
      source_url = "https://storage.googleapis.com/${google_storage_bucket.bucket.name}/${google_storage_bucket_object.object.name}"
    }
  }

  env_variables = {
    port = "8080"
  }

  delete_service_on_destroy = true
}