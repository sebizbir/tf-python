output "aws_lb_dns_name" {
  value = aws_lb.ecs_load_balancer_generic.dns_name
}
output "aws_lb_zone_id" {
  value = aws_lb.ecs_load_balancer_generic.zone_id
}
