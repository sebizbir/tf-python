# ecs cluster
variable "ecs_cluster_name" {}
variable "ecs_enable_container_insights" {}
# ecs service
variable "sg_ec2_app" {}
variable "ecs_service_name_generic" {}
variable "ecs_service_desired_count_generic" {}
variable "iam_role_ecs_service" {}
# ecs task definitions
variable "ecs_task_definition_generic" {}
variable "ecr_generic_name" {}
# vpc
variable "vpc_id" {}
variable "env_id" {}
variable "aws_region" {}
variable "public_subnet_cidr_az1" {}
variable "public_subnet_cidr_az2" {}
variable "private_subnet_cidr_az1_internal_app" {}
variable "private_subnet_cidr_az2_internal_app" {}
variable "private_subnet_cidr_az1_internal_data" {}
variable "private_subnet_cidr_az2_internal_data" {}
# alb
variable "ecs_load_balancer_generic_name" {}
variable "sg_ecs_load_balancer" {}
variable "lambda_static_website" {}
variable "s3_static_website_name" {}
variable "statefile_bucket_domain_name" {}