##################################################################
#                     ECS Generic ALB                            #
##################################################################
resource "aws_lb" "ecs_load_balancer_generic" {
  name               = var.ecs_load_balancer_generic_name
  internal           = false
  load_balancer_type = "application"
  security_groups    = [var.sg_ecs_load_balancer]
  subnets            = [var.public_subnet_cidr_az1, var.public_subnet_cidr_az2]
  tags          = {
    Name        = var.ecs_load_balancer_generic_name
    Environment = var.env_id
  }

  depends_on = [var.sg_ecs_load_balancer,var.public_subnet_cidr_az1, var.public_subnet_cidr_az2]
}

##################################################################
#                     ECS Generic TG                             #
##################################################################
resource "aws_lb_target_group" "ecs_load_balancer_tg_generic_main" {
  name     = "ecs-main-lb-tg"
  port     = 80
  protocol = "HTTP"
  vpc_id   = var.vpc_id

  depends_on = [var.vpc_id]
}

resource "aws_lb_listener" "ecs_generic_http" {
  load_balancer_arn = aws_lb.ecs_load_balancer_generic.id
  port              = "80"
  protocol          = "HTTP"

  default_action {
    target_group_arn = aws_lb_target_group.ecs_load_balancer_tg_generic_main.id
    type             = "forward"
  }

  depends_on        = [aws_lb.ecs_load_balancer_generic,aws_lb_target_group.ecs_load_balancer_tg_generic_main]
}
##################################################################
#                     Lambda target group                        #
##################################################################
// Allow LB assignment
resource "aws_lambda_permission" "lambda_with_lb" {
  statement_id  = "AllowExecutionFromlb"
  action        = "lambda:InvokeFunction"
  function_name = var.lambda_static_website
  principal     = "elasticloadbalancing.amazonaws.com"
  source_arn    = aws_lb_target_group.lambda_static_load_balancer_tg.arn

  depends_on = [aws_lb_target_group.lambda_static_load_balancer_tg]
}

resource "aws_lb_target_group" "lambda_static_load_balancer_tg" {
  name        = "lambda-static-lb-tg"
  target_type = "lambda"
}

resource "aws_lb_listener_rule" "lambda_forward_rule" {
  listener_arn = aws_lb_listener.ecs_generic_http.arn
  priority     = 100
  action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.lambda_static_load_balancer_tg.arn
  }

  condition {
    path_pattern {
      values = ["/lambda/*"]
    }
  }

  depends_on = [aws_lb_listener.ecs_generic_http,aws_lb_target_group.lambda_static_load_balancer_tg]
}

// Assign lambda to lb
resource "aws_lb_target_group_attachment" "lb_lambda" {
  target_group_arn = aws_lb_target_group.lambda_static_load_balancer_tg.arn
  target_id        = var.lambda_static_website

  depends_on       = [aws_lb_target_group.lambda_static_load_balancer_tg,aws_lambda_permission.lambda_with_lb]
}

##################################################################
#                  S3 redirect target group                      #
##################################################################
// redirect to s3 website url
resource "aws_lb_listener_rule" "redirect_path_to_s3" {
  listener_arn = aws_lb_listener.ecs_generic_http.arn

  action {
    type = "redirect"

    redirect {
      host        = "${var.s3_static_website_name}.${var.statefile_bucket_domain_name}"
      path        = "/index.html"
      query       = ""
      status_code = "HTTP_301"
    }
  }

  condition {
    path_pattern {
      values = ["/bucket/*"]
    }
  }

  depends_on = [aws_lb_listener.ecs_generic_http]
}

// redirect to route53 record which leads to redirect to s3 bucket
resource "aws_lb_listener_rule" "redirect_path_to_route53" {
  listener_arn = aws_lb_listener.ecs_generic_http.arn

  action {
    type = "redirect"

    redirect {
      host        = var.s3_static_website_name
      path        = "/index.html"
      query       = ""
      status_code = "HTTP_301"
    }
  }

  condition {
    path_pattern {
      values = ["/route/*"]
    }
  }

  depends_on = [aws_lb_listener.ecs_generic_http]
}



/*

// redirect to cloud functions
resource "aws_lb_listener_rule" "redirect_path_to_google_function" {
  listener_arn = aws_lb_listener.ecs_generic_http.arn

  action {
    type = "redirect"

    redirect {
      protocol    = "HTTPS"
      port        = "443"
      host        = "us-central1-igneous-effort-257218.cloudfunctions.net"
      path        = "/function-1"
      query       = ""
      status_code = "HTTP_301"
    }
  }

  condition {
    path_pattern {
      values = ["/google/*"]
    }
  }

  depends_on = [aws_lb_listener.ecs_generic_http]
}
*/