/*
resource "aws_ecs_task_definition" "service" {
  family                    = var.ecs_task_definition_generic
  requires_compatibilities  = "EC2"
  container_definitions     = file("task-definitions/service.json")
  task_role_arn             = ""
  #network_mode              = ""     #empty for default
  #cpu                       = ""     #for Fargate
  #memory                    = ""     #for Fargate

  volume {
    name      = "service-storage"
    host_path = "/ecs/service-storage"
  }
  placement_constraints {
    type       = "memberOf"
    expression = "attribute:ecs.availability-zone in [eu-west-2a, eu-west-2b]"
  }
  tags 					= {
    Name 				= var.ecs_cluster_name
    Environment         = var.env_id
  }
}
*/