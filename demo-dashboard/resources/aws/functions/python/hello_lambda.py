def lambda_handler(event, context):
    response = {
        "statusCode": 200,
        "headers": {"Content-Type": "text/html; charset=utf-8"},
        "body": "<h1>Welcome to Lambda static webpage</h1>"
    }
    return response


