output "lambda_static_website_arn" {
  value = aws_lambda_function.lambda_static_website.arn
}
output "lambda_telegram_bot_arn" {
  value = aws_lambda_function.lambda_telegram_bot.arn
}