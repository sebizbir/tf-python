#############################
#       Static website      #
#############################
data "archive_file" "static_zip" {
  type        = "zip"
  source_file = "../../resources/aws/functions/python/hello_lambda.py"
  output_path = "../../resources/aws/functions/python/hello_lambda.zip"
}

resource "aws_lambda_function" "lambda_static_website" {
  function_name = "lambda_static_website"
  role          = var.iam_lambda_static_arn
  handler       = "hello_lambda.lambda_handler"

  filename         = data.archive_file.static_zip.output_path
  source_code_hash = data.archive_file.static_zip.output_base64sha256

  runtime       = "python3.6"
  environment {
    variables   = {
      greeting  = "Hello"
    }
  }
}
#############################
#       Telegram bot        #
#############################
data "archive_file" "telegram_zip" {
  type        = "zip"
  source_file = "../../resources/aws/functions/python/telegram_lambda.py"
  output_path = "../../resources/aws/functions/python/telegram_lambda.zip"
}

resource "aws_lambda_function" "lambda_telegram_bot" {
  function_name = "lambda_telegram_bot"
  role          = var.iam_lambda_telegram_arn
  handler       = "telegram_lambda.lambda_handler"

  filename         = data.archive_file.telegram_zip.output_path
  source_code_hash = data.archive_file.telegram_zip.output_base64sha256

  runtime       = "python3.6"
  environment {
    variables   = {
      TOKEN     = var.lambda_telegram_bot_token
      USER_ID   = var.lambda_telegram_bot_user_id
    }
  }
}