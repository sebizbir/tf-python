variable "cdn_demo_enabled" {}
variable "cdn_demo_default_root_object" {}
variable "cdn_demo_price_class" {}
variable "statefile_bucket_domain_name" {}
variable "gcp_domain_name" {}
variable "s3_static_website_name" {}
variable "acm_aws_demo_dashboard_arn" {}
