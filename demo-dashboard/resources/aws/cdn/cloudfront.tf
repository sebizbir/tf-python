locals {
  s3_origin_id = "s3_static_website"
}

resource "aws_cloudfront_origin_access_identity" "cdn_demo_static_website_oai" {
  comment = "OAI for static website"
}

// !!!! takes up to 15 minutes
resource "aws_cloudfront_distribution" "cdn_demo_static_website" {

  enabled             = var.cdn_demo_enabled
  default_root_object = var.cdn_demo_default_root_object
  price_class         = var.cdn_demo_price_class

  //for route 53 alias is necessary, for alias ACM certificate is necessary
  //aliases = [var.gcp_domain_name]

  origin {

    domain_name = "${var.s3_static_website_name}.${var.statefile_bucket_domain_name}"
    origin_id   = local.s3_origin_id

    custom_origin_config {
      http_port              = "80"
      https_port             = "443"
      origin_protocol_policy = "http-only"
      origin_ssl_protocols   = ["TLSv1", "TLSv1.1", "TLSv1.2"]
    }
    //s3_origin_config {
    //  origin_access_identity = aws_cloudfront_origin_access_identity.cdn_demo_static_website_oai.cloudfront_access_identity_path
    //}
  }

  viewer_certificate {
    cloudfront_default_certificate = true
    //acm_certificate_arn = var.acm_aws_demo_dashboard_arn
   // iam_certificate_id =
  }

  default_cache_behavior {
    allowed_methods  = ["DELETE", "GET", "HEAD", "OPTIONS", "PATCH", "POST", "PUT"]
    cached_methods   = ["GET", "HEAD"]
    target_origin_id = local.s3_origin_id

    forwarded_values {
      headers = ["Origin"]

      query_string = false

      cookies {
        forward           =  "none"
      }
    }

    viewer_protocol_policy = "allow-all"
  }

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }
 // for quick terraform process change to 'true' and delete cloudfront manually
  retain_on_delete = false

  depends_on = [var.s3_static_website_name, aws_cloudfront_origin_access_identity.cdn_demo_static_website_oai]
}