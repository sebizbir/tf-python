# ECS
output "iam_role_ecs_service" {
  value = aws_iam_role.ecs_service.id
}
# Lambda
output "iam_lambda_static" {
  value = aws_iam_role.iam_lambda_static.id
}
output "iam_lambda_static_arn" {
  value = aws_iam_role.iam_lambda_static.arn
}
output "iam_lambda_telegram" {
  value = aws_iam_role.iam_lambda_telegram.id
}
output "iam_lambda_telegram_arn" {
  value = aws_iam_role.iam_lambda_telegram.arn
}