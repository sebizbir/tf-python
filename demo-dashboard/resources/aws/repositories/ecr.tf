##################################################################
#                         ECR repository                         #
##################################################################
resource "aws_ecr_repository" "ecr_generic" {
  name                 = var.ecr_generic_name
  image_tag_mutability = var.ecr_generic_mutability

  image_scanning_configuration {
    scan_on_push = true
  }
  tags = {
    Name        = var.ecr_generic_name
    Environment = var.env_id
  }
}
##################################################################
#                     ECR repository IAM policy                  #
##################################################################
resource "aws_ecr_repository_policy" "ecr_generic_policy" {
  repository = aws_ecr_repository.ecr_generic.name

  depends_on = [aws_ecr_repository.ecr_generic]

  policy = <<EOF
{
    "Version": "2008-10-17",
    "Statement": [
        {
            "Sid": "new policy",
            "Effect": "Allow",
            "Principal": "*",
            "Action": [
                "ecr:GetDownloadUrlForLayer",
                "ecr:BatchGetImage",
                "ecr:BatchCheckLayerAvailability",
                "ecr:PutImage",
                "ecr:InitiateLayerUpload",
                "ecr:UploadLayerPart",
                "ecr:CompleteLayerUpload",
                "ecr:DescribeRepositories",
                "ecr:GetRepositoryPolicy",
                "ecr:ListImages",
                "ecr:DeleteRepository",
                "ecr:BatchDeleteImage",
                "ecr:SetRepositoryPolicy",
                "ecr:DeleteRepositoryPolicy"
            ]
        }
    ]
}
EOF
}
##################################################################
#               ECR repository Lifecycle policy                  #
##################################################################
###############################
#       Untagged images       #
###############################
resource "aws_ecr_lifecycle_policy" "ecr_generic_lc_policy_untagged" {
  repository = aws_ecr_repository.ecr_generic.name

  depends_on = [aws_ecr_repository.ecr_generic]

  policy = <<EOF
{
    "rules": [
        {
            "rulePriority": 1,
            "description": "Expire images older than 14 days",
            "selection": {
                "tagStatus": "untagged",
                "countType": "sinceImagePushed",
                "countUnit": "days",
                "countNumber": 14
            },
            "action": {
                "type": "expire"
            }
        }
    ]
}
EOF
}
###############################
#        Tagged images        #
###############################
resource "aws_ecr_lifecycle_policy" "ecr_generic_lc_policy_tagged" {
  repository = aws_ecr_repository.ecr_generic.name

  depends_on = [aws_ecr_repository.ecr_generic]

  policy = <<EOF
{
    "rules": [
        {
            "rulePriority": 1,
            "description": "Keep last 30 images",
            "selection": {
                "tagStatus": "tagged",
                "tagPrefixList": ["v"],
                "countType": "imageCountMoreThan",
                "countNumber": 30
            },
            "action": {
                "type": "expire"
            }
        }
    ]
}
EOF
}
