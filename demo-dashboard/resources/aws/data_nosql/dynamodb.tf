#################################################################################
#                                NoSQL DynamoDB tables                          #
#################################################################################
resource "aws_dynamodb_table" "demo_dashboard_dynamo_users" {
  name           = "Userdata"
  billing_mode   = "PROVISIONED"
  read_capacity  = 20
  write_capacity = 20
  hash_key       = "EmployeeId"
  range_key      = "Department"

  attribute {
    name = "EmployeeId"
    type = "S"
  }

  attribute {
    name = "Department"
    type = "S"
  }

  attribute {
    name = "Salary"
    type = "N"
  }

  ttl {
    attribute_name = "TimeToExist"
    enabled        = false
  }

  global_secondary_index {
    name               = "DepartmentIndex"
    hash_key           = "Department"
    range_key          = "Salary"
    write_capacity     = 10
    read_capacity      = 10
    projection_type    = "INCLUDE"
    non_key_attributes = ["EmployeeId"]
  }

  tags = {
    Name        = "Userdata"
    Environment = var.env_id
  }
}