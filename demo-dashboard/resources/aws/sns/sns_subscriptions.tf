##########################
# SNS trigger for Lambda #
##########################
resource "aws_sns_topic_subscription" "generic_sns_subscription" {
  topic_arn   = aws_sns_topic.telegram_sns_topic.arn
  protocol    = "lambda"
  endpoint    = var.lambda_telegram_bot_arn

  depends_on  = [aws_sns_topic.telegram_sns_topic]
}
##########################
#  lambda:InvokeFunction #
##########################
resource "aws_lambda_permission" "with_sns" {
  statement_id  = "AllowExecutionFromSNS"
  action        = "lambda:InvokeFunction"
  function_name = var.lambda_telegram_bot_arn
  principal     = "sns.amazonaws.com"
  source_arn    = aws_sns_topic.telegram_sns_topic.arn

  depends_on    = [aws_sns_topic.telegram_sns_topic]
}