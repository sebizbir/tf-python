##########################
# SNS topic for Telegram #
##########################
resource "aws_sns_topic" "telegram_sns_topic" {
  name              = var.generic_sns_topic_name
  tags 				= {
    Name 			= var.generic_sns_topic_name
  }
}