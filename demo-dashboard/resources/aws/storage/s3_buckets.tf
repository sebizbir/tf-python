#############################
#      Static S3 website    #
#############################
resource "aws_s3_bucket" "statefile_bucket" {
  region        = var.aws_region
  bucket        = var.s3_static_website_name
  force_destroy = true

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "PublicReadGetObject",
      "Effect": "Allow",
      "Principal": "*",
      "Action": "s3:GetObject",
      "Resource": "arn:aws:s3:::${var.s3_static_website_name}/*"
    }
  ]
}
EOF

  website {
    index_document = "index.html"
    error_document = "error.html"
  }

  cors_rule {
    allowed_headers = ["*"]
    allowed_methods = ["PUT", "POST"]
    allowed_origins = ["*"]
    expose_headers  = ["ETag"]
    max_age_seconds = 3000
  }
}
#############################
#   Files for S3 website    #
#############################
resource "aws_s3_bucket_object" "static_index_file" {
  bucket        = var.s3_static_website_name
  key           = "index.html"
  source        = "../../resources/aws/storage/static_website/index.html"
  content_type  = "text/html"

  depends_on    = [aws_s3_bucket.statefile_bucket]
}

resource "aws_s3_bucket_object" "static_error_file" {
  bucket        = var.s3_static_website_name
  key           = "error.html"
  source        = "../../resources/aws/storage/static_website/error.html"
  content_type  = "text/html"

  depends_on    = [aws_s3_bucket.statefile_bucket]
}

resource "aws_s3_bucket_object" "static_styles_file" {
  bucket        = var.s3_static_website_name
  key           = "styles.css"
  source        = "../../resources/aws/storage/static_website/styles.css"
  content_type  = "text/css"

  depends_on    = [aws_s3_bucket.statefile_bucket]
}
