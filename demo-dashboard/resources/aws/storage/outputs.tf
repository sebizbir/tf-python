output "statefile_bucket_domain_name" {
  value = aws_s3_bucket.statefile_bucket.website_domain
}
output "statefile_bucket_zone_id" {
  value = aws_s3_bucket.statefile_bucket.hosted_zone_id
}