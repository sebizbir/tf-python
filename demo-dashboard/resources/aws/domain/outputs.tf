####### Route53
output "aws_demo_dashboard_zone_id" {
  value = aws_route53_zone.aws_zone_demo_dashboard.zone_id
}
output "aws_demo_dashboard_name_servers" {
  value = aws_route53_zone.aws_zone_demo_dashboard.name_servers
}
# acm
output "acm_aws_demo_dashboard_arn" {
  value = aws_acm_certificate.acm_aws_demo_dashboard.arn
}