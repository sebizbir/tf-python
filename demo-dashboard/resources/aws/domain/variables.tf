# ACM
variable "demo_dashboard_validate_acm" {}
variable "env_id" {}
variable "gcp_domain_name" {}
variable "domain_name" {}
# Route53
variable "aws_lb_dns_name" {}
variable "aws_lb_zone_id" {}
# S3
variable "s3_static_website_name" {}
variable "statefile_bucket_domain_name" {}
variable "statefile_bucket_zone_id" {}
# Cloudfront
variable "cdn_demo_static_website_domain_name" {}
variable "cdn_demo_static_website_zone_id" {}