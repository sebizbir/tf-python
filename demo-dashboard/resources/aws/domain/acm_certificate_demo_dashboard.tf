#########################################################################
#                        ACM SSL certificate for domain                 #
#########################################################################
resource "aws_acm_certificate" "acm_aws_demo_dashboard" {
  validation_method = "DNS"
  domain_name       = var.domain_name

  subject_alternative_names = [
    var.domain_name,
    "www.${var.domain_name}",
    var.s3_static_website_name,
    "www.${var.s3_static_website_name}",
  //  "cdn.${var.domain_name}"
  ]

  tags          = {
    Domain      = var.domain_name
    Environment = var.env_id
  }

  lifecycle {
    create_before_destroy = true
  }

  depends_on    = [aws_route53_zone.aws_zone_demo_dashboard]
}

#########################################################################
#                 ACM SSL certificate validation in Route53             #
#########################################################################
resource "aws_route53_record" "demo_dashboard_validation_main" {
  //count = var.demo_dashboard_validate_acm ? 1 : 0

  name    = aws_acm_certificate.acm_aws_demo_dashboard.domain_validation_options[0].resource_record_name
  type    = aws_acm_certificate.acm_aws_demo_dashboard.domain_validation_options[0].resource_record_type
  zone_id = aws_route53_zone.aws_zone_demo_dashboard.zone_id
  records = [aws_acm_certificate.acm_aws_demo_dashboard.domain_validation_options[0].resource_record_value]
  ttl     = "60"

  depends_on = [aws_acm_certificate.acm_aws_demo_dashboard, aws_route53_zone.aws_zone_demo_dashboard]
}
resource "aws_route53_record" "demo_dashboard_validation_alt_1" {
  //count = var.demo_dashboard_validate_acm ? 1 : 0

  name    = aws_acm_certificate.acm_aws_demo_dashboard.domain_validation_options[1].resource_record_name
  type    = aws_acm_certificate.acm_aws_demo_dashboard.domain_validation_options[1].resource_record_type
  zone_id = aws_route53_zone.aws_zone_demo_dashboard.zone_id
  records = [aws_acm_certificate.acm_aws_demo_dashboard.domain_validation_options[1].resource_record_value]
  ttl     = "60"

  depends_on = [aws_acm_certificate.acm_aws_demo_dashboard, aws_route53_zone.aws_zone_demo_dashboard]
}
resource "aws_route53_record" "demo_dashboard_validation_alt_2" {
  //count = var.demo_dashboard_validate_acm ? 1 : 0

  name    = aws_acm_certificate.acm_aws_demo_dashboard.domain_validation_options[2].resource_record_name
  type    = aws_acm_certificate.acm_aws_demo_dashboard.domain_validation_options[2].resource_record_type
  zone_id = aws_route53_zone.aws_zone_demo_dashboard.zone_id
  records = [aws_acm_certificate.acm_aws_demo_dashboard.domain_validation_options[2].resource_record_value]
  ttl     = "60"

  depends_on = [aws_acm_certificate.acm_aws_demo_dashboard, aws_route53_zone.aws_zone_demo_dashboard]
}
resource "aws_route53_record" "demo_dashboard_validation_alt_3" {
  //count = var.demo_dashboard_validate_acm ? 1 : 0

  name    = aws_acm_certificate.acm_aws_demo_dashboard.domain_validation_options[3].resource_record_name
  type    = aws_acm_certificate.acm_aws_demo_dashboard.domain_validation_options[3].resource_record_type
  zone_id = aws_route53_zone.aws_zone_demo_dashboard.zone_id
  records = [aws_acm_certificate.acm_aws_demo_dashboard.domain_validation_options[3].resource_record_value]
  ttl     = "60"

  depends_on = [aws_acm_certificate.acm_aws_demo_dashboard, aws_route53_zone.aws_zone_demo_dashboard]
}

// cloudfront cdn
/*
resource "aws_route53_record" "demo_dashboard_validation_alt_4" {
  //count = var.demo_dashboard_validate_acm ? 1 : 0

  name    = aws_acm_certificate.acm_aws_demo_dashboard.domain_validation_options[4].resource_record_name
  type    = aws_acm_certificate.acm_aws_demo_dashboard.domain_validation_options[4].resource_record_type
  zone_id = aws_route53_zone.aws_zone_demo_dashboard.zone_id
  records = [aws_acm_certificate.acm_aws_demo_dashboard.domain_validation_options[4].resource_record_value]
  ttl     = "60"

  depends_on = [aws_acm_certificate.acm_aws_demo_dashboard, aws_route53_zone.aws_zone_demo_dashboard]
}
*/
##################################
#           AWS - Route53        #
##################################
/*

// !!!! takes up to 45 minutes for external domain

resource "aws_acm_certificate_validation" "acm_validation_aws" {
  //count = var.demo_dashboard_validate_acm ? 1 : 0
  certificate_arn         = aws_acm_certificate.acm_aws_demo_dashboard.arn
  validation_record_fqdns = [
    aws_route53_record.demo_dashboard_validation_main.fqdn,
    aws_route53_record.demo_dashboard_validation_alt_1.fqdn,
    aws_route53_record.demo_dashboard_validation_alt_2.fqdn,
    aws_route53_record.demo_dashboard_validation_alt_3.fqdn,
    aws_route53_record.demo_dashboard_validation_alt_4.fqdn
  ]

  depends_on    = [
    aws_acm_certificate.acm_aws_demo_dashboard,
    aws_route53_record.demo_dashboard_validation_main,
    aws_route53_record.demo_dashboard_validation_alt_1,
    aws_route53_record.demo_dashboard_validation_alt_2,
    aws_route53_record.demo_dashboard_validation_alt_3,
    aws_route53_record.demo_dashboard_validation_alt_4
  ]
}
*/