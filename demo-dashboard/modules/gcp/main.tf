#########################################################################
#                     IAM - Users and service accounts                  #
#########################################################################
module "iam_api" {
  source                                  = "../../resources/gcp/iam_api/"
  # Project data
  demo_project_id                         = var.demo_project_id
  demo_project_number                     = var.demo_project_number
}

#########################################################################
#                     Network - VPC, CIDR's, Subnets                    #
#########################################################################
module "network" {
  source                                  = "../../resources/gcp/network/"
  enable_ssl                              = var.enable_ssl
  gcp_demo_dashboard_api_compute          = module.iam_api.api_compute
  demo_project_id                         = var.demo_project_id
  demo_project_region                     = var.demo_project_region
  gcp_domain_name                         = var.gcp_domain_name
  # VPC data
  network_name                            = var.network_name
  vpc_subnetworks                         = var.vpc_subnetworks
  # Load balancers
  statefile_bucket_name                   = module.storage.statefile_bucket_name
}
#########################################################################
#                       Security - Firewall rules                       #
#########################################################################
/*
module "security" {
  source                                  = "../../resources/gcp/security/"
  # VPC data
  network_name                            = var.network_name
  # FW - http/https
  generic_http_https_allow_protocol       = var.generic_http_https_allow_protocol
  generic_http_https_allow_protocol_ports = var.generic_http_https_allow_protocol_ports
  generic_http_https_firewall_name        = var.generic_http_https_firewall_name
  generic_http_https_source_ranges        = var.generic_http_https_source_ranges
  generic_http_https_source_tags          = var.generic_http_https_source_tags
  generic_http_https_target_tags          = var.generic_http_https_target_tags
  # FW - ssh
  generic_ssh_allow_protocol              = var.generic_ssh_allow_protocol
  generic_ssh_allow_protocol_ports        = var.generic_ssh_allow_protocol_ports
  generic_ssh_firewall_name               = var.generic_ssh_firewall_name
  generic_ssh_source_ranges               = var.generic_ssh_source_ranges
  generic_ssh_source_tags                 = var.generic_ssh_source_tags
  generic_ssh_target_tags                 = var.generic_ssh_target_tags
}
*/
#########################################################################
#                         Storage - Storage buckets                     #
#########################################################################
module "storage" {
  source                                  = "../../resources/gcp/storage/"
  # Storage buckets
  statefile_bucket_name                   = var.statefile_bucket_name
  statefile_bucket_location               = var.statefile_bucket_location
  statefile_bucket_class                  = var.statefile_bucket_class

  functions_bucket_name                   = var.functions_bucket_name
  functions_bucket_location               = var.functions_bucket_location
  functions_bucket_class                  = var.functions_bucket_class
}
#########################################################################
#                            Cloud Functions                            #
#########################################################################
module "functions" {
  source                                  = "../../resources/gcp/functions/"
  terraform_service_account_email         = module.iam_api.terraform_service_account_email
  gcp_demo_dashboard_api_cloudfunctions   = module.iam_api.api_cloudfunctions
  # VPC data
  env_id                                  = var.env_id
  demo_project_id                         = var.demo_project_id
  demo_project_region                     = var.demo_project_region
  # Functions data
  functions_bucket_name                   = module.storage.functions_bucket_name
  functions_bucket_object_1_name          = module.storage.functions_bucket_object_1_name
}
#########################################################################
#                            Domain - Cloud DNS                         #
#########################################################################
module "domain" {
  source                                  = "../../resources/gcp/domain/"
  gcp_demo_dashboard_api_dns              = module.iam_api.api_dns
  demo_dashboard_lb_http_address          = module.network.demo_dashboard_lb_http_address
  cloud_run_demo_dashboard_main_url       = module.compute_run.cloud_run_demo_dashboard_main_url
  cloud_run_demo_dashboard_domain_mapping = module.compute_run.cloud_run_demo_dashboard_domain_mapping[0]
  # VPC data
  env_id                                  = var.env_id
  gcp_domain_name                         = var.gcp_domain_name
}

#########################################################################
#                           Compute - Cloud run                         #
#########################################################################
module "compute_run" {
  source                                  = "../../resources/gcp/compute_run/"
  # VPC data
  demo_project_id                                        = var.demo_project_id
  gcp_domain_name                                        = var.gcp_domain_name
  # CloudRun data
  cloud_run_demo_dashboard_service_name                  = var.cloud_run_demo_dashboard_service_name
  cloud_run_demo_dashboard_service_location              = var.cloud_run_demo_dashboard_service_location
  cloud_run_demo_dashboard_service_namespace             = var.cloud_run_demo_dashboard_service_namespace
  cloud_run_demo_dashboard_service_image                 = var.cloud_run_demo_dashboard_service_image
}
#########################################################################
#                           Compute - App Engine                        #
#########################################################################
module "compute_app_engine" {
  source                                  = "../../resources/gcp/compute_app_engine/"
  # Single Instances data
  demo_project_id                                        = var.demo_project_id
  demo_project_region                                   = var.demo_project_region
}
#########################################################################
#                           Compute - Instances                         #
#########################################################################
module "compute_instances" {
  source                                  = "../../resources/gcp/compute_instances/"
  # Single Instances data
  instance_generic_name                   = var.instance_generic_name
  instance_generic_type                   = var.instance_generic_type
  instance_generic_zone                   = var.instance_generic_zone
  instance_generic_tags                   = var.instance_generic_tags
  instance_generic_network                = module.network.network_generic_name
  # Instances
  instance_group_generic_name             = var.instance_group_generic_name
  # VPC data
  demo_project_id                                        = var.demo_project_id
}
#########################################################################
#                      Data - SQL Databases (MySQL)                     #
#########################################################################
module "data_sql" {
  source                                  = "../../resources/gcp/data_sql/"
  # CloudSQL - demo_dashboard_mysql
  api_cloud_sql                                  = module.iam_api.api_cloud_sql
  api_service_networking                         = module.iam_api.api_service_networking
  # DB Instance
  demo_dashboard_mysql_database_instance_name    = var.demo_dashboard_mysql_database_instance_name
  demo_dashboard_mysql_database_version          = var.demo_dashboard_mysql_database_version
  demo_dashboard_mysql_database_region           = var.demo_dashboard_mysql_database_region
  demo_dashboard_mysql_database_tier             = var.demo_dashboard_mysql_database_tier
  demo_dashboard_mysql_database_ipv4_enabled     = var.demo_dashboard_mysql_database_ipv4_enabled
  demo_dashboard_mysql_database_private_network  = module.network.demo_dashboard_network_id
  # Database
  demo_dashboard_mysql_database_name             = var.demo_dashboard_mysql_database_name
  # Users
  demo_dashboard_mysql_sql_user_name             = var.demo_dashboard_mysql_sql_user_name
  demo_dashboard_mysql_sql_user_password         = var.demo_dashboard_mysql_sql_user_password
}
#########################################################################
#                      Data - NoSQL Databases (Datastore)               #
#########################################################################
