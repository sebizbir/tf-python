#########################################################################
#                                Default                                #
#########################################################################
variable "demo_project_id" {
  description = "Project ID"
  #default     = "ambient-shelter-264215"
  default     = "soy-antenna-264311"
}

variable "demo_project_region" {
  default = "europe-west2"
}

variable "demo_project_number" {
  description = "Project Number"
  #default     = "181449428943"
  default      = "733577469189"
}
#########################################################################
#                                Network                                #
#########################################################################
#########################################################
#                          VPC                          #
#########################################################
variable "network_name" {
  description = "Name for VPC network"
  default     = "demo-dashboard"
}

variable "vpc_subnetworks" {
  default = true
}

variable "env_id" {
  description   = "Environment name"
  default       = "demo"
}

variable "enable_ssl" {
  description = "true/false value for enable SSL"
  type = bool
  default = true
}
#########################################################
#                           IAM                         #
#########################################################

#########################################################
#                        Firewall                       #
#########################################################
/*
variable "generic_ssh_firewall_name" {}
variable "generic_http_https_firewall_name" {}
variable "generic_ssh_allow_protocol" {}
variable "generic_ssh_allow_protocol_ports" {}
variable "generic_http_https_allow_protocol" {}
variable "generic_http_https_allow_protocol_ports" {}
variable "generic_http_https_target_tags" {}
variable "generic_http_https_source_ranges" {}
variable "generic_http_https_source_tags" {}
variable "generic_ssh_target_tags" {}
variable "generic_ssh_source_ranges" {}
variable "generic_ssh_source_tags" {}
*/
#########################################################################
#                                DOMAIN                                 #
#########################################################################
variable "gcp_domain_name" {
  description = "Domain name for a GCP project and SSL certificate."
  default     = "gcp-dashboard.ml"
}
#########################################################################
#                                Compute                                #
#########################################################################
#########################################################
#                       Instances                       #
#########################################################
/*
# Single instances
variable "instance_generic_name" {}
variable "instance_generic_type" {}
variable "instance_generic_zone" {}
variable "instance_generic_tags" {}
# Instance groups
variable "instance_group_generic_name" {}
*/
#########################################################
#                       CloudRun                       #
#########################################################
variable "cloud_run_demo_dashboard_service_name" {
  default = "demo-dashboard-cloudrun"
  description = "Name of Cloud Run service"
}
variable "cloud_run_demo_dashboard_service_location" {
  default = "europe-west1"
  description = "Region for Cloud Run service (europe-west2 not supported)"
}
variable "cloud_run_demo_dashboard_service_namespace" {
  default = "soy-antenna-264311"
  description = "Namespace must be equal to either the project ID or project number"
}
variable "cloud_run_demo_dashboard_service_image" {
  description = "Name of the docker image to deploy."
  default = "gcr.io/soy-antenna-264311/helloworld"
}
#########################################################
#                       App Engine                      #
#########################################################
#########################################################################
#                                Storage                                #
#########################################################################
#########################################################
#                        Buckets                        #
#########################################################
variable "statefile_bucket_name" {
  description = "Name for a bucket, should be same as hosted zone for Route53: variable domain_name"
  default = "bucket.gcp-dashboard.ml"
}
variable "statefile_bucket_location" {
  description = "The GCS location"
  default = "EUROPE-WEST2"
}
variable "statefile_bucket_class" {
  description = "The Storage Class of the bucket. Supported values include: STANDARD, MULTI_REGIONAL, REGIONAL, NEARLINE, COLDLINE."
  default = "STANDARD"
}

variable "functions_bucket_name" {
  description = "Name for a bucket, should be same as hosted zone for Route53: variable domain_name"
  default = "gcp-demo-dashboard-functions"
}
variable "functions_bucket_location" {
  description = "The GCS location"
  default = "EU"
}
variable "functions_bucket_class" {
  description = "Name for a bucket, should be same as hosted zone for Route53: variable domain_name"
  default = "STANDARD"
}

#########################################################################
#                             Data - SQL                                #
#########################################################################
#########################################################
#                          MySQL                        #
#########################################################
# CloudSQL - demo_dashboard_mysql
# DB Instance
variable "demo_dashboard_mysql_database_instance_name" {
  default = "demo-dashboard-master-instance"
}
variable "demo_dashboard_mysql_database_version" {
  default = "MYSQL_5_7"
}
variable "demo_dashboard_mysql_database_region" {
  default = "europe-west2"
}
variable "demo_dashboard_mysql_database_tier" {
  default = "db-f1-micro"
}
variable "demo_dashboard_mysql_database_ipv4_enabled" {
  default = true
}
# Database
variable "demo_dashboard_mysql_database_name" {
  default = "demo-dashboard"
}
# Users
variable "demo_dashboard_mysql_sql_user_name" {
  default = "admin"
}
variable "demo_dashboard_mysql_sql_user_password" {
  default = "R@nd0m"
}
