provider "google" {
  #credentials = file("~/.gcp/ambient-shelter-264215-ef50c949199a.json")
  credentials = file("~/.gcp/soy-antenna-264311-78c568a802a5.json")
  project     = var.demo_project_id
  region      = var.demo_project_region
  version = ">= 3.3"
}

provider "google-beta" {
  #credentials = file("~/.gcp/ambient-shelter-264215-ef50c949199a.json")
  credentials = file("~/.gcp/soy-antenna-264311-78c568a802a5.json")
  project     = var.demo_project_id
  region      = var.demo_project_region
}

