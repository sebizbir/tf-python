output "demo_project_id" {
  value = var.demo_project_id
}

output "terraform_service_account_email" {
  value = module.iam_api.terraform_service_account_email
}

output "gcp_demo_dashboard_name_servers" {
  value = module.domain.gcp_demo_dashboard_name_servers
}
