#########################################################################
#                                DEFAULT                                #
#########################################################################
#variable "aws_access_key" {}
#variable "aws_secret_key" {}
#variable "aws_key_path" {}
#variable "aws_key_name" {}

variable "aws_region" {
  description 	= "EC2 Region for the VPC"
  default 		= "eu-central-1"
}

variable "env_id" {
  description   = "Environment name"
  default       = "Demo-dashboard Environment"
}
#########################################################################
#                                NETWORK                                #
#########################################################################
#########################################################
#                         VPC                           #
#########################################################
variable "vpc_cidr" {
  description 	= "CIDR for the VPC"
  default 		= "10.63.0.0/22"
}
variable "vpc_dns" {
  description   = "Enable dns hostnames"
  default       = true
}
#########################################################
#                         Subnets                       #
#########################################################
################################################
#                   Public                     #
################################################
variable "public_subnet_cidr_az1" {
  description 	= "CIDR for the Public Subnet - External Subnet - AZ1"
  default 		= "10.63.1.0/26"
}
variable "public_subnet_cidr_az2" {
  description 	= "CIDR for the Public Subnet - External Subnet - AZ2"
  default 		= "10.63.1.64/26"
}
################################################
#                  Private                     #
################################################
variable "private_subnet_cidr_az1_internal_app" {
  description 	= "CIDR for the Private App Subnet - Internal Subnet - AZ1"
  default 		= "10.63.2.0/26"
}
variable "private_subnet_cidr_az2_internal_app" {
  description 	= "CIDR for the Private App Subnet - Internal Subnet - AZ2"
  default 		= "10.63.2.64/26"
}
variable "private_subnet_cidr_az1_internal_data" {
  description 	= "CIDR for the Private Data Subnet - Internal Subnet - AZ1"
  default 		= "10.63.3.0/26"
}
variable "private_subnet_cidr_az2_internal_data" {
  description 	= "CIDR for the Private Data Subnet - Internal Subnet - AZ2"
  default 		= "10.63.3.64/26"
}
#########################################################################
#                             SECURITY                                  #
#########################################################################
variable "sg_ecs_load_balancer_name" {
  description = "Security group name for Application Load balancer"
  default     = "sg_ecs_load_balancer"
}
variable "sg_app_instances_name" {
  description = "Security group name for Application instances"
  default     = "sg_app_instances_name"
}
variable "sg_rds_mssql_database_name" {
  description = "Security group name for RDS MSSQL database"
  default     = "sg_rds_mssql_database"
}
variable "sg_rds_mysql_database_name" {
  description = "Security group name for RDS MySQL database"
  default     = "sg_rds_mysql_database"
}
#########################################################################
#                                DOMAIN                                 #
#########################################################################
#####################################
#              ACM/DNS              #
#####################################
variable "demo_dashboard_validate_acm" {
  description = "true/false value for enabling DNS validation in ACM/Route53"
  type = bool
  default = false
}
//  Domain from freenom.com. Should be created prior to project creation and NS records from Route53 added to Freenom
variable "domain_name" {
  description = "Domain name for a project and SSL certificate."
  default     = "aws-dashboard.ml"
}
variable "gcp_domain_name" {
  description = "Domain name for a GCP project and SSL certificate."
  default     = "gcp-demo-dashboard.tk"
}

#########################################################################
#                               STORAGE                                 #
#########################################################################
variable "s3_static_website_name" {
  description = "Name for a bucket, should be same as hosted zone for Route53: variable domain_name"
  default = "bucket.aws-dashboard.ml"
}
#########################################################################
#                                  CDN                                  #
#########################################################################
variable "cdn_demo_enabled" {
  description = "Whether the distribution is enabled to accept end user requests for content."
  default = true
}
variable "cdn_demo_default_root_object" {
  description = "The object that you want CloudFront to return (for example, index.html) when an end user requests the root URL."
  default = "index.html"
}
variable "cdn_demo_price_class" {
  description = "PriceClass_All - all regions, PriceClass_200 - USA, Europe, HK, Singapore and Japan, PriceClass_100 - USA and Europe"
  default = "PriceClass_100"
}
#########################################################################
#                                COMPUTE                                #
#########################################################################
####################################################################
#                               ECS                                #
#  https://www.terraform.io/docs/providers/aws/r/ecs_service.html  #
####################################################################
variable "ecs_cluster_name" {
  description = "Name for ECS cluster"
  default      = "GenericECS"
}
variable "ecs_enable_container_insights" {
  description = "Enabled/Disabled, container Insights for ECS cluster"
  default 	  = "Enabled"
}
variable "ecs_service_desired_count_generic" {
  description = "Desired count of running tasks for ECS service"
  default     = 1
}
variable "ecs_service_name_generic" {
  description = "Name for ECS service"
  default     = "generic_service"
}
variable "ecs_task_definition_generic" {
  description = "Name for ECS Task definition"
  default     = "generic_task_definition"
}
variable "ecs_load_balancer_generic_name" {
  description = "Name for ECS Application Load balancer"
  default     = "ecs-lb"
}
#########################################################################
#                             REPOSITORIES                              #
#########################################################################
variable "ecr_generic_name" {
  description = "Name for ECR repository"
  default     = "ecr-demo-dashboard"
}
variable "ecr_generic_mutability" {
  description = "ECR repository mutability"
  default     = "MUTABLE"
}
#########################################################################
#                           SNS notifications                           #
#########################################################################
variable "generic_sns_topic_name" {
  description = "SNS topic name"
  default     = "TelegramSNSTopic"
}
#########################################################################
#                             Lambda functions                          #
#########################################################################
variable "lambda_telegram_bot_token" {
  description = "Token for Telegram bot: t.me/Notifier_Telegram_bot"
  default     = "974449979:AAH-n-WsxaPh9h-bS-GVZNAldNk-RYho5bs"
}
variable "lambda_telegram_bot_user_id" {
  description = "User ID for Telegram bot: t.me/Notifier_Telegram_bot"
  default     = "394754492"
}
#########################################################################
#                               DATA - SQL                              #
#     https://www.terraform.io/docs/providers/aws/r/db_instance.html    #
#########################################################################
#####################################
#               MSSQL               #
#####################################
variable "rds_mssql_identifier" {
  description = "Database identifier"
  default     = "terraform-generic"
}
variable "rds_mssql_allocated_storage" {
  description = "Allocated storage in GB"
  default     = 30
}
variable "rds_mssql_backup_retention_period" {
  description = "Retention period"
  default     = 7
}
variable "rds_mssql_backup_window" {
  description = "Backup window (should be >=30 mins)"
  default     = "01:37-02:07"
}
variable "rds_mssql_maintenance_window" {
  description = "Maintenance window (should be >=30 mins)"
  default     = "Mon:23:29-Mon:23:59"
}
variable "rds_mssql_multi_az" {
  description = "Enable/disable Multi AZ failover option"
  default     = true
}
variable "rds_mssql_storage_encrypted" {
  description = "Encrypt/Not encrypt underlying storage"
  default     = true
}
variable "rds_mssql_publicly_accessible" {
  description = "Enable public/private access"
  default    = false
}
variable "rds_mssql_storage_type" {
  description = "Underlying storage type"
  default     = "gp2"
}
variable "rds_mssql_engine" {
  description = "MSSQL engine type"
  default     = "sqlserver-se"
}
variable "rds_mssql_engine_version" {
  description = "MSSQL engine version"
  default     = "13.00.5292.0.v1"
}
variable "rds_mssql_license_model" {
  description = "MSSQL license model"
  default     = "license-included"
}
variable "rds_mssql_instance_class" {
  description = "RDS instance class for the underlying machine"
  default     = "db.m5.xlarge"
}
variable "rds_mssql_username" {
  description = "Initial user"
  default     = "dbGeneric"
}
variable "rds_mssql_password" {
  description = "Initial password"
  default     = "dbGeneric2019"
}
#####################################
#               MySQL               #
#####################################
variable "rds_mysql_identifier" {
  description = "Database identifier"
  default     = "terraform-generic"
}
variable "rds_mysql_allocated_storage" {
  description = "Allocated storage in GB"
  default     = 20
}
variable "rds_mysql_backup_retention_period" {
  description = "Retention period"
  default     = 7
}
variable "rds_mysql_backup_window" {
  description = "Backup window (should be >=30 mins)"
  default     = "01:37-02:07"
}
variable "rds_mysql_maintenance_window" {
  description = "Maintenance window (should be >=30 mins)"
  default     = "Mon:23:29-Mon:23:59"
}
variable "rds_mysql_multi_az" {
  description = "Enable/disable Multi AZ failover option"
  default     = true
}
variable "rds_mysql_storage_encrypted" {
  description = "Encrypt/Not encrypt underlying storage"
  default     = true
}
variable "rds_mysql_publicly_accessible" {
  description = "Enable public/private access"
  default     = false
}
variable "rds_mysql_storage_type" {
  description = "Underlying storage type"
  default     = "gp2"
}
variable "rds_mysql_engine" {
  description = "MySQL engine type"
  default     = "mysql"
}
variable "rds_mysql_engine_version" {
  description = "MySQL engine version"
  default     = "5.5.61"
}
variable "rds_mysql_instance_class" {
  description = "RDS instance class for the underlying machine"
  default     = "db.m5.xlarge"
}
variable "rds_mysql_db_name" {
  description = "MySQL database name"
  default     = "dbCAGateway"
}
variable "rds_mysql_username" {
  description = "Initial user"
  default     = "dbCAGateway"
}
variable "rds_mysql_password" {
  description = "Initial password"
  default     = "dbCAGateway2019"
}
variable "rds_mysql_family_name" {
  description = "MySQL database family name"
  default     = "mysql5.5"
}