#########################################################################
#                     Network - VPC, CIDR's, Subnets                    #
#########################################################################
module "network" {
  source                                      = "../../resources/aws/network/"
  # VPC data
  aws_region                                  = var.aws_region
  env_id                                      = var.env_id
  vpc_dns                                     = var.vpc_dns
  vpc_cidr                                    = var.vpc_cidr
  # Subnets
  public_subnet_cidr_az1                      = var.public_subnet_cidr_az1
  public_subnet_cidr_az2                      = var.public_subnet_cidr_az2
  private_subnet_cidr_az1_internal_data       = var.private_subnet_cidr_az1_internal_data
  private_subnet_cidr_az2_internal_data       = var.private_subnet_cidr_az2_internal_data
  private_subnet_cidr_az1_internal_app        = var.private_subnet_cidr_az1_internal_app
  private_subnet_cidr_az2_internal_app        = var.private_subnet_cidr_az2_internal_app
}
#########################################################################
#                             IAM  - IAM roles                          #
#########################################################################
module "iam" {
  source                                      = "../../resources/aws/iam/"
}
#########################################################################
#                       Security - Security groups                      #
#########################################################################
// depends on network module
module "security" {
  source                                      = "../../resources/aws/security/"
  # VPC data
  vpc_id                                      = module.network.vpc_id
  # Security groups
  sg_ecs_load_balancer_name                   = var.sg_ecs_load_balancer_name
  sg_app_instances_name                       = var.sg_app_instances_name
  sg_rds_mssql_database_name                  = var.sg_rds_mssql_database_name
  sg_rds_mysql_database_name                  = var.sg_rds_mysql_database_name
}
#########################################################################
#                        Repositories - ECR                             #
#########################################################################
module "repositories" {
  source                                      = "../../resources/aws/repositories/"
  # VPC data
  env_id                                      = var.env_id
  # ECR data
  ecr_generic_name                            = var.ecr_generic_name
  ecr_generic_mutability                      = var.ecr_generic_mutability
}
#########################################################################
#                         Storage - S3 buckets                          #
#########################################################################
module "storage" {
  source                                      = "../../resources/aws/storage/"
  # VPC data
  aws_region                                  = var.aws_region
  # S3 data
  s3_static_website_name                      = var.s3_static_website_name
}
#########################################################################
#                            CDN - S3 buckets                           #
#########################################################################
// depends on storage, acm, domain modules
module "cdn" {
  source                                      = "../../resources/aws/cdn/"
  # S3 data
  s3_static_website_name                      = var.s3_static_website_name
  gcp_domain_name                             = var.gcp_domain_name
  cdn_demo_enabled                            = var.cdn_demo_enabled
  cdn_demo_default_root_object                = var.cdn_demo_default_root_object
  cdn_demo_price_class                        = var.cdn_demo_price_class
  statefile_bucket_domain_name                = module.storage.statefile_bucket_domain_name
  acm_aws_demo_dashboard_arn                  = module.domain.acm_aws_demo_dashboard_arn
}
#########################################################################
#                      Data - Databases (MSSQL, MySQL)                  #
#########################################################################
// depends on network, security modules
module "data_sql" {
  source                                      = "../../resources/aws/data_sql/"
  # Subnets
  private_subnet_cidr_az1_internal_data       = module.network.private_subnet_cidr_az1_internal_data
  private_subnet_cidr_az2_internal_data       = module.network.private_subnet_cidr_az2_internal_data
  # Security groups
  sg_rds_mssql_database                       = module.security.sg_rds_mssql_database
  sg_rds_mysql_database                       = module.security.sg_rds_mysql_database
  # MSSQL RDS data
  rds_mssql_identifier                        = var.rds_mssql_identifier
  rds_mssql_allocated_storage                 = var.rds_mssql_allocated_storage
  rds_mssql_backup_retention_period           = var.rds_mssql_backup_retention_period
  rds_mssql_backup_window                     = var.rds_mssql_backup_window
  rds_mssql_maintenance_window                = var.rds_mssql_maintenance_window
  rds_mssql_multi_az                          = var.rds_mssql_multi_az
  rds_mssql_storage_encrypted                 = var.rds_mssql_storage_encrypted
  rds_mssql_publicly_accessible               = var.rds_mssql_publicly_accessible
  rds_mssql_storage_type                      = var.rds_mssql_storage_type
  rds_mssql_engine                            = var.rds_mssql_engine
  rds_mssql_engine_version                    = var.rds_mssql_engine_version
  rds_mssql_license_model                     = var.rds_mssql_license_model
  rds_mssql_instance_class                    = var.rds_mssql_instance_class
  rds_mssql_username                          = var.rds_mssql_username
  rds_mssql_password                          = var.rds_mssql_password
  # Mysql RDS data
  rds_mysql_db_name                           = var.rds_mysql_db_name
  rds_mysql_family_name                       = var.rds_mysql_family_name
  rds_mysql_identifier                        = var.rds_mysql_identifier
  rds_mysql_allocated_storage                 = var.rds_mysql_allocated_storage
  rds_mysql_backup_retention_period           = var.rds_mysql_backup_retention_period
  rds_mysql_backup_window                     = var.rds_mysql_backup_window
  rds_mysql_maintenance_window                = var.rds_mysql_maintenance_window
  rds_mysql_multi_az                          = var.rds_mysql_multi_az
  rds_mysql_storage_encrypted                 = var.rds_mysql_storage_encrypted
  rds_mysql_publicly_accessible               = var.rds_mysql_publicly_accessible
  rds_mysql_storage_type                      = var.rds_mysql_storage_type
  rds_mysql_engine                            = var.rds_mysql_engine
  rds_mysql_engine_version                    = var.rds_mysql_engine_version
  rds_mysql_instance_class                    = var.rds_mysql_instance_class
  rds_mysql_username                          = var.rds_mysql_username
  rds_mysql_password                          = var.rds_mysql_password
}
#########################################################################
#                      Data - Databases (NoSQL, DynamoDB                #
#########################################################################
// depends on network, security modules
module "data_nosql" {
  source                                      = "../../resources/aws/data_nosql/"
  # VPC data
  env_id                                      = var.env_id
}
#########################################################################
#                     SNS - SNS topics and subscriptions                #
#########################################################################
// depends on functions module
module "sns" {
  source                                      = "../../resources/aws/sns/"
  # SNS data
  generic_sns_topic_name                      = var.generic_sns_topic_name
  # Endpoints
  lambda_telegram_bot_arn                     = module.functions.lambda_telegram_bot_arn
}
#########################################################################
#                         Functions - Lambdas                           #
#########################################################################
// depends on iam module
module "functions" {
  source                                      = "../../resources/aws/functions/"
  # IAM
  iam_lambda_static_arn                       = module.iam.iam_lambda_static_arn
  iam_lambda_telegram_arn                     = module.iam.iam_lambda_telegram_arn
  # Telegram
  lambda_telegram_bot_token                   = var.lambda_telegram_bot_token
  lambda_telegram_bot_user_id                 = var.lambda_telegram_bot_user_id
}
#########################################################################
#     Compute ECS - ECS Cluster, Services, Tasks and Load balancer      #
#########################################################################
// depends on network, iam, security, storage, functions modules
module "compute_ecs" {
  source                                      = "../../resources/aws/compute_ecs/"
  # VPC data
  env_id                                      = var.env_id
  vpc_id                                      = module.network.vpc_id
  aws_region                                  = var.aws_region
  # IAM
  iam_role_ecs_service                        = module.iam.iam_role_ecs_service
  # ECS data
  ecs_cluster_name                            = var.ecs_cluster_name
  ecs_enable_container_insights               = var.ecs_enable_container_insights
  ecs_service_desired_count_generic           = var.ecs_service_desired_count_generic
  ecs_service_name_generic                    = var.ecs_service_name_generic
  ecs_task_definition_generic                 = var.ecs_task_definition_generic
  ecr_generic_name                            = module.repositories.ecr_generic_name
  # LB
  ecs_load_balancer_generic_name              = var.ecs_load_balancer_generic_name
  lambda_static_website                       = module.functions.lambda_static_website_arn
  s3_static_website_name                      = var.s3_static_website_name
  statefile_bucket_domain_name                = module.storage.statefile_bucket_domain_name
  # Security
  sg_ec2_app                                  = module.security.sg_ec2_app
  sg_ecs_load_balancer                        = module.security.sg_ecs_load_balancer
  # Subnets
  public_subnet_cidr_az1                      = module.network.public_subnet_cidr_az1
  public_subnet_cidr_az2                      = module.network.public_subnet_cidr_az2
  private_subnet_cidr_az1_internal_app        = module.network.private_subnet_cidr_az1_internal_app
  private_subnet_cidr_az2_internal_app        = module.network.private_subnet_cidr_az2_internal_app
  private_subnet_cidr_az1_internal_data       = module.network.private_subnet_cidr_az1_internal_data
  private_subnet_cidr_az2_internal_data       = module.network.private_subnet_cidr_az2_internal_data
}
#########################################################################
#                        SSL - ACM and Route53                          #
#########################################################################
// depends on compute, storage, cdn modules
module "domain" {
  source                                      = "../../resources/aws/domain/"
  demo_dashboard_validate_acm                 = var.demo_dashboard_validate_acm
  # VPC data
  env_id                                      = var.env_id
  # ACM data
  gcp_domain_name                             = var.gcp_domain_name
  domain_name                                 = var.domain_name
  # Route53 data
  aws_lb_dns_name                             = module.compute_ecs.aws_lb_dns_name
  aws_lb_zone_id                              = module.compute_ecs.aws_lb_zone_id
  # S3 data
  s3_static_website_name                      = var.s3_static_website_name
  statefile_bucket_domain_name                = module.storage.statefile_bucket_domain_name
  statefile_bucket_zone_id                    = module.storage.statefile_bucket_zone_id
  # Cloudfront
  cdn_demo_static_website_domain_name         = module.cdn.cdn_demo_static_website_domain_name
  cdn_demo_static_website_zone_id             = module.cdn.cdn_demo_static_website_zone_id
}

