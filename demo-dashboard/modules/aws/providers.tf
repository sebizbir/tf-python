provider "aws" {
  //shared_credentials_file = "/c/Users/Main/.aws/credentials"
  profile                 = "terraform"
  region      = var.aws_region
}