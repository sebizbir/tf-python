# Scripts with modules (reusable infrastructure)
# tf-python
Repository for terraform config files, Jenkinsfile, Gitlab CI and sphinx

## Prerequisites
To use this repo you have to ensure all of the values passed into the initilisation jobs and gitlab variables are correct + that your access keys are generated and stored as SECURE variables

## Info

Terraform scripts wre split into 2 folders based on provisioning type - modules and resources. Each folder consists of cloud service type: `aws`, `gcp`
And on a lower level resources folder consists of folders with module types:  `network`, `security`, `compute` etc.

##### Modules:
Files - modules:

| File | Description |
| --- | --- |
| `main.tf` | Modules creation |
| `providers.tf` | Provider definition |
| `variables.tf` | Variables/parameters for resources |
| `terraform.tfvars` | Variables |
| `terraform.tfstate` | Current state of the environment |
| `terraform.tfstate.backup` | Current state of the environment (backup) |

##### Resources:
Files - resources:

| File | Description |
| --- | --- |
| `variables.tf` | Variables/parameters for resources |
| `outputs.tf` | Outputs from resources to use in other modules |
| `*service*.tf` | Services which will be created in a module |

## Useful Documentation
[Terraform AWS Provider Documentation](https://www.terraform.io/docs/providers/aws/)

[Terraform GCP Provider Documentation](https://www.terraform.io/docs/providers/google/index.html)
